<?php $page = "altacostura-aac2019"; ?>
<?php $padre = "altacostura"; ?>
<?php include '../../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../../templates/meta.php'; ?>
	<?php include '../../../templates/favicon.php'; ?>
	<title>Aquarelle Coleccion alta costura 2018 -19 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="<?= ROOT_PATH ?>css/altacostura_aac2019.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
</head>

<body>
	<?php include '../../../templates/header.php'; ?>
	<div id="page" class="bg_alpaca">
		<div class="hero herocolaac2019 wow fadeIn">
			<!-- <img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg"> -->
			<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg" />
			<h1>AQUARELLE <br> <span>Coleccion alta costura 2019</span> </h1>
		</div>
		<main id="main" class=container>
			<div class="doble_medium_especial doble  box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg" />
				<img id=smallimage data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06_cerca.jpg" />
			</div>
			<div class="medium_grand left box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/28.jpg" />

			</div>
			<div class="doble_medium_especial doble box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg" />
				<img id=smallimage data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/29.jpg" />
			</div>
			<div id="grid" class="box-space">
				<img class="wow bounceInLeft" data-wow-delay="0.2s" id="img01" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg" />
				<img class="wow bounceInDown" data-wow-delay="0.3s" id="img02" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg" />
				<img class="wow bounceInUp" data-wow-delay="0.4s" id="img03" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/27.jpg" />
			</div>
			<div class="medium_grand complet box-space wow  fadeIn">
				<img class="wow bounceInLeft" data-wow-delay="0.2s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg" />
			</div>

			<div class="doble_medium_especial doble box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/30.jpg" />
				<img id=smallimage data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/31.jpg" />
			</div>
			<div class="medium_grand complet box-space wow  fadeIn">
				<img class="wow bounceInLeft" data-wow-delay="0.2s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/32.jpg" />
			</div>
			<div class="medium_grand grand left box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/33.jpg" />
			</div>
			<div class="medium_grand right box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg" />
			</div>
			<div class="doble_medium_especial doble box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/34.jpg" />
				<img id=smallimage data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/35.jpg" />
			</div>
			<div class="ver-coleccion container-zoom box-space">
				<div class="medium_grand complet box-space wow  fadeIn">
					<a href="detalle1"><img class="wow bounceInLeft" data-wow-delay="0.2s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/36.jpg" /></a>
				</div>
				<div class="container-ver-mas">
					<a class="btn-ver-mas" href="detalle1">Ver galeria de fotos</a>
				</div>
			</div>

			<div class="medium_grand complet box-space wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/26.jpg" />
			</div>
			<p>ver video del desfile aquarelle</p>
			<p>ver video del backstage aquarelle</p>
		</main>
	</div>
	<?php include '../../../templates/footer.php'; ?>
	<?php include '../../../templates/scriptBeforeBody.php'; ?>
	<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
		<script>
		new WOW().init();
		</script>	 -->
</body>

</html>