<?php $page = "altacostura"; ?>
<?php $padre = "altacostura"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?>
	<title>Sección Alta Costura - Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../templates/cssjs.php';?>
	<link type="text/css" rel="stylesheet" href="<?=ROOT_PATH?>css/portada.css">
</head>

<body class="portada">
	<div id="mainWrapper">
		<?php include '../../templates/header.php';?>
		<aside id="aside">
			<h1>ALTA COSTURA</h1>
			<nav class="jd-vertical-menu">
				<a href="aquarelle-2019-20">AQUARELLE  ALTA COSTURA 2019 - 20</a>
				<a href="sumacwarmin-2018-19">SUMAC WARMI  DE ALTA COSTURA 2018 -19</a>
				<a href="ophelia-2018">ALTA COSTURA 2017-18</a>
			</nav>
		</aside>
		<main id="main">
			<img id="jdPortadaImg" src="<?=ROOT_PATH?>images/moda/portada.jpg">
		</main>
		<?php include '../../templates/footer.php';?>
	</div>

</body>
</html>