<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="description" content="Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="abstract" content="Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="keywords" content="Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:site_name" content="Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta itemprop="name" content="Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<link rel="shortcut icon" href="../../images/favicon.png" type="image/png" />
  <title>Vestidos de fiesta y coctel | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 

<link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/Aller_Bold-normal-bold/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/Aller_Light-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/Aller_Light_Italic-italic-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/Dotum-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/Halo_handletter-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/Neoretrofill-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/Notera-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../../blog/sites/default/files/fontyourface/local_fonts/rage_italic-italic-normal/stylesheet.css">

<link type="text/css" rel="stylesheet" href="../../css/global.css">
<link rel="stylesheet" href="../../css/jquery.lightbox.css" />

<script src="../../js/jquery-1.11.3.min.js"></script>
<script src="../../js/jquery.bxslider.js"></script>
<link href="../../css/jquery.bxslider.css" rel="stylesheet" />

<link rel="stylesheet" href="../../css/jquery.slinky.css" />
<script src="../../js/jquery.slinky.js"></script>
<!-- Facebook Pixel Code -->
<script>

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;

n.push=n;n.loaded=!0;n.version=’2.0′;n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,

document,’script’,’https://connect.facebook.net/en_US/fbevents.js’);

fbq(‘init’, ‘276600206390559‘, {

em: ‘insert_email_variable,’

});

fbq(‘track’, ‘PageView’);

</script>
<nonscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=276600206390559&ev=PageView&noscript=1"/>
</nonscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="html not-front fiestadet">

<div class="page clearfix" id="page">

<header id="section-header" class="section section-header">
  <div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">  
  <div id="zone-branding" class="zone zone-branding clearfix ">
    <div class=" region region-branding" id="region-branding">
		<div class="region-inner region-branding-inner">
				


		</div>
	</div>
  </div>
  </div>
</header>

<section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix ">    
        
        <div class=" region region-content" id="region-content">
			  <div class="region-inner region-content-inner clearfix">

				<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
					<div class="block-inner clearfix">
						<div class="content clearfix">
						
						<!--contenido-->
						
						<h1 class="page-title">ALTA COSTURA 2017 - 18 PASARELA "OPHELIA ET LES FLEURS"</h1>
						
						<!--menu movil-->
						<span class="mmovil"></span>
						<div class="block-menumovil" id="menumovil" style="display:none;">
        				  <ul class="menumovil">
        					    <li><a href="../index">Inicio</a></li>
        						<li><a href="../novias">NOVIAS</a></li>
        						<li><a href="../vestidos-de-fiesta-coctel">ALTA COSTURA 2017 - 18</a></li>
        					    <li><a href="../colecciones">COLECCIONES</a></li>
        					    <li><a href="../joyas">JOYAS</a></li>
        					    <li class="blog"><a href="../../blog/es">Blog</a></li>
        					</ul>
        				  
        				</div>
						<!--fin menu movil-->
						
						<div class="block-breadcrumb" id="breadcrumb">
				  <div class="block-inner clearfix">
					<ul class="slimmenu">
					    <li><a href="../index">Jenny Duarte</a></li>
						<li><a href="../vestidos-de-fiesta-coctel">ALTA COSTURA 2017 - 18</a></li>
						<li class="last">PASARELA "OPHELIA ET LES FLEURS"</li>
					</ul>
				  </div>
				</div>
				
				
					<div class="pagdet">
					    <div class="burbuja">
					    <a href="detalle1">1</a>
					    <a href="detalle2" class="active">2</a>
					    <a href="detalle3">3</a>
					    <a href="detalle4">4</a>
					    	</div>
					    	<div class="tit">ALTA COSTURA 2017 - 18</div>
					    	</div>
						<!-- slider -->
        	  			<ul class="bxslider">
        	  			  <li><a class="lightb" href="images/7.jpg"><img src="images/7.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/8.jpg"><img src="images/8.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/9.jpg"><img src="images/9.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/10.jpg"><img src="images/10.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/11.jpg"><img src="images/11.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/12.jpg"><img src="images/12.jpg" /></a></li>
        	  			</ul>
        	  			
        	  			<div id="bx-pager">
                          <a data-slide-index="0" href=""><img src="images/7.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="1" href=""><img src="images/8.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="2" href=""><img src="images/9.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="3" href=""><img src="images/10.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="4" href=""><img src="images/11.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="5" href=""><img src="images/12.jpg" /><span class="hover"></span></a>
                        </div>
        	  			<!-- End slider -->
						
						
						<!--fin contenido-->
						
						</div>
					</div>
				</div>
				
				</div>
	
		</div>
	</div>
	</div>
</section>    
  
<footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix ">
    <div class=" region region-footer-first" id="region-footer-first">
		<div class="region-inner region-footer-first-inner clearfix">
			
			<div class="block block-locale  block-language block-locale-language odd block-without-title" id="block-locale-language">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<ul class="language-switcher-locale-url">
							<li class="en first"><a href="../../en/vestidos-de-fiesta-coctel/detalle2" class="language-link ">English</a></li>
							<li class="es last active"><a href="detalle2" class="language-link active" xml:lang="es">Español</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="block block-nice-menus  block-2 block-nice-menus-2 even block-without-title" id="block-nice-menus-2">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<ul class="nice-menu nice-menu-down nice-menu-menu-menu-secundario" id="nice-menu-2">
							<li class="menu odd first"><a href="../trayectoria-profesional" title="">Biografía</a></li>
							<li class="menu even "><a href="../moda-sustentable">Moda Sustentable</a></li>
							<li class="menu odd last"><a href="../contacto" title="Dejanos un mensaje.">Contacto</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="block block-block  block-2 block-block-2 odd block-without-title" id="block-block-2">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<p class="face im"><a href="https://www.pinterest.com" target="_blank">
							<img alt="" src="../../images/icono_pinterest.png"  /></a>
						</p>
						
						<p class="face im"><a href="https://www.twitter.com" target="_blank">
							<img alt="" src="../../images/twitter.png"  /></a>
						</p>
						
						<p class="face im"><a href="https://es-es.facebook.com/jennyduartemoda" target="_blank">
							<img alt="" src="../../images/face.png" /></a>
						</p>
						
							<p class="face im"><a href="https://instagram.html" target="_blank">
							<img alt="" src="../../images/instagram.png" /></a>
						</p>
						
						<p class="comprar im"><a href="http://web.iasoftgroup.com/jenny2017/es/cart">
							<img alt="" src="../../images/tienda.png"  /></a>
						</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
  </div>
  </div>
</footer>  

</div>

<script src="../../js/jquery.slimmenu.js"></script>
<script src="../../js/jquery.easing.min.js"></script>
<script src="../../js/jquery.lightbox.min.js"></script>
<script>
$(document).ready(function(){

var slinky = $('#menumovil').slinky();


$(".mmovil").click(function(){
        $(this).toggleClass("open");
        $("#menumovil").toggleClass("abrir");
        if($("#menumovil").hasClass("abrir")){
            $("#menumovil").css("display","block");
        }
        else{
            $("#menumovil").css("display","none");
        }
    });

$('.bxslider').bxSlider({
mode:'fade',
  infiniteLoop: true,
useCSS:true,
touchEnabled:true,
auto:true,
speed:700,
adaptiveHeight:true,
  responsive: true,
pagerCustom: '#bx-pager'
});

$(window).load(function() {

var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}else{
$("#zone-content-wrapper").css("padding-top","").css("padding-bottom","");
}

$("li a.mostrar").click(function(){
        $(this).parent().find("ul").toggleClass("abrir")
    });
    $(".cerrar").click(function(){
        $("li ul.abrir").removeClass("abrir")
    });
    
}); //fin load

$(window).resize(function() {

	var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
	if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}else{
$("#zone-content-wrapper").css("padding-top","").css("padding-bottom","");
}

}); //fin resize

$('.lightb').lightbox();

});//fin ready

</script>
</body>
</html>