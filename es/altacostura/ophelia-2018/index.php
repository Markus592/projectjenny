<?php $page = "altacostura"; ?>
<?php include '../../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
    <?php include '../../../templates/meta.php'; ?>
    <?php include '../../../templates/favicon.php'; ?>
    <title>Coleccion de Alta costura 2017 -18 | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title>
    <?php include '../../../templates/fontsBlog.php'; ?>
    <?php include '../../../templates/cssjsDetalleColeccion.php'; ?>
</head>

<?php include '../../../templates/header.php'; ?>

<body class="html not-front vestidosfiesta">

    <div class="page clearfix" id="page">

        <header id="section-header" class="section section-header">
            <div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">
                <div id="zone-branding" class="zone zone-branding clearfix ">
                    <div class=" region region-branding" id="region-branding">
                        <div class="region-inner region-branding-inner">



                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section id="section-content" class="section section-content">
            <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">
                <div id="zone-content" class="zone zone-content clearfix ">

                    <div class=" region region-content" id="region-content">
                        <div class="region-inner region-content-inner">

                            <div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
                                <div class="block-inner clearfix">
                                    <div class="content clearfix">

                                        <!--contenido-->

                                        <!-- <h1 class="page-title">Vestidos de Fiesta & Coctel</h1> -->

                                        <!--menu movil-->
                                        <!-- <span class="mmovil"></span> -->
                                        <!-- <div class="block-menumovil" id="menumovil" style="display:none;"> -->
                                            <!-- <ul class="menumovil"> -->
                                                <!-- <li><a href="../index">Inicio</a></li> -->
                                                <!-- <li><a href="../novias">NOVIAS</a></li> -->
                                                <!-- <li><a href="../vestidos-de-fiesta-coctel">VESTIDOS DE FIESTA & COCTEL</a></li> -->
                                                <!-- <li><a href="../colecciones">COLECCIONES</a></li> -->
                                                <!-- <li><a href="../joyas">JOYAS</a></li> -->
                                                <!-- <li class="blog"><a href="../../blog/es">Blog</a></li> -->
                                            <!-- </ul> -->
<!--  -->
                                        <!-- </div> -->
                                        <!--fin menu movil-->

                                        <div class="block-breadcrumb" id="breadcrumb">
                                            <div class="block-inner clearfix">
                                                <ul class="slimmenu">
                                                    <li><a href="../index">Jenny Duarte</a></li>
                                                    <li class="last">Alta Costura 2017 - 18</li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="portada"><img src="images/portadav1.jpg">
                                            <div class="flechaselector"> &nbsp;</div>
                                        </div>
                                        <div class="divisorespacios">&nbsp;</div>

                                        <div class="bloquef1 bloque">
                                            <div class="inner">
                                                <div class="imagen1 imagen"><img src="images/coctel18.jpg"></div>
                                                <div class="imagen2 imagen"><img src="images/ophelia2.jpg"></div>
                                            </div>
                                        </div>
                                        <div class="divisorespacios">&nbsp;</div>
                                        <!-- slider -->
                                        <div class="frase"> Pasarela "Ophelia et les fleurs" <a href="detalle1"><span class="detallefrase"> Fotos del Desfile</span></a></div>
                                        <ul class="bxslider">
                                            <!--<li><a href="detalle1"><img src="images/portadav1.jpg" /></a></li>-->
                                            <li><a href="detalle1"><img src="images/ophelia.jpg" /></a></li>
                                        </ul>
                                        <!-- End slider -->
                                        <div class="divisorespacios">&nbsp;</div>
                                        <div class="bloquef2 bloque">
                                            <div class="inner">
                                                <div class="imagen3 imagen"><img src="images/modelo2.png"></div>
                                                <div class="video">
                                                    <iframe src="https://player.vimeo.com/video/217593733" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="divisorespacios">&nbsp;</div>
                                        <div class="bloquef3 bloque">
                                            <div class="imagen4 imagen"><img src="images/fiesta1_4.jpg"></div>
                                        </div>
                                        <div class="divisorespacios">&nbsp;</div>
                                        <div class="bloquef2_1 bloque">
                                            <div class="inner">
                                                <div class="imagen1 imagen"><img src="images/modelo3.png"></div>
                                                <div class="texto">
                                                    Jenny Duarte presenta “Ophelia et les fleurs” colección de Alta Costura 2017, una propuesta de vestidos de fiesta, cocktel, 15 años, promoción y novias, esta línea es dedicada para las mujeres que quieren sentirse únicas y auténticas.



                                                </div>
                                                <div class="imagen1 imagen imgmod1"><img src="images/modelo1.png"></div>
                                            </div>
                                        </div>
                                        <div class="divisorespacios">&nbsp;</div>
                                        <div class="bloquef2_2 bloque">
                                            <div class="imagen4 imagen"><img src="images/vestidorojo.jpg"></div>
                                        </div>
                                        <div class="divisorespacios">&nbsp;</div>
                                        <!-- slider -->
                                        <ul class="bxslider">
                                            <li><img class="mitad" src="images/vestido1.jpg" /></li>
                                            <li><img class="mitad" src="images/vestido2.jpg" /></li>
                                            <li><img class="mitad" src="images/vestido3.jpg" /></li>
                                            <li><img class="mitad" src="images/vestido4.jpg" /></li>
                                            <li><img class="mitad" src="images/vestido5.jpg" /></li>
                                        </ul>
                                        <!-- End slider -->
                                        <div class="divisorespacios">&nbsp;</div>
                                        <!-- slider -->
                                        <div class="ultimas">
                                            <img src="images/vestidorojo4.jpg" />
                                        </div>
                                        <!-- End slider -->


                                        <!--fin contenido-->

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>

        

    </div>
    <?php include '../../../templates/footer.php'; ?>
    <?php include '../../../templates/scriptBeforeBody.php'; ?>
</body>

</html>