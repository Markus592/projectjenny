<?php $page = "altacostura-swdac2018"; ?>
<?php $padre = "altacostura"; ?>
<?php include '../../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../../templates/meta.php'; ?>
	<?php include '../../../templates/favicon.php'; ?>
	<title>SUMAC WARMI Colección de alta costura 2018 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="<?= ROOT_PATH ?>css/altacostura_swdac2018.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css">
 -->
</head>

<body>
	<?php include '../../../templates/header.php'; ?>
	<div id="page" class=bg_alpaca>
		<div class="hero herosumwar2018 wow fadeIn" id="hero_especialswdac">
			<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg">
			<h1>SUMAC WARMI <br>
				<span>Coleccion de alta costura 2018</span>
			</h1>
		</div>
		<main id="main" class="container">
			<br>
			<div id="grid1" class="box-space">
				<img class="wow bounceInLeft" data-wow-delay="0.2s" id="img01" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg">
				<img class="wow bounceInDown" data-wow-delay="0.3s" id="img02" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg">
				<img class="wow bounceInUp" data-wow-delay="0.4s" id="img03" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg">
			</div>
			<div class="doble_medium box-space doble">
				<img class="wow  fadeInLeft img05" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg">
				<img class="wow  fadeInRight img06" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg">
			</div>
			<div class="divisorbanner nofloat">
				<ul class="bxslider">
					<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg" /></li>
					<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/08.jpg" /></li>
					<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg" /></li>
				</ul>
			</div>
			<div class="banner">
				<div class="divisorbanner nofloat">
					<ul class="bxslider">
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg" /></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg" /></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/20.jpg" /></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/21.jpg" /></li>
					</ul>
				</div>
			</div>
			<div class="medium_grand wow box-space  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/22.jpg">
			</div>
			<div class="medium_grand wow left fadeIn box-space">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/47.jpg">
			</div>
			<div class="ver-coleccion box-space container-zoom">
				<div class="medium_grand coleccionsumac1 wow  fadeIn" id="portada2_23">
					<a href="detalle1"><img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada2_23.jpg"></a>
				</div>
				<div class="container-ver-mas">
					<a class="btn-ver-mas" href="detalle1">Ver galeria de fotos</a>
				</div>
			</div>
			<div class="medium_grand right wow fadeIn box-space">
				<img class="wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/48.jpg">
			</div>
			<div class="banner ">
				<div class="divisorbanner sliderheightlarge nofloat">
					<ul class="bxslider">
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14.jpg" /></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg" /></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/16.jpg" /></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg" /></li>
					</ul>
				</div>
			</div>
			<div class="wow  fadeIn box-space" id="grid2">
				<img class="wow bounceInLeft" data-wow-delay="0.2s" id="img10" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg">
				<img class="wow bounceInDown" data-wow-delay="0.3s" id="img11" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg">
				<img class="wow bounceInUp" data-wow-delay="0.4s" id="img12" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg">
			</div>
			<div class="doble_medium_especial box-space doble">
				<img class="wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/23.jpg">
				<img class="wow  fadeInRight" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/24.jpg">
			</div>
			<p style="font-size: 5rem;">falta aqui video</p>
		</main>
	</div>
	<?php include '../../../templates/footer.php'; ?>
	<?php include '../../../templates/scriptBeforeBody.php'; ?>
	<!-- <script>
					new WOW().init();	
				</script>  -->
</body>

</html>