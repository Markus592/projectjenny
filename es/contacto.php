<?php $page = "contacto"; ?>
<?php include '../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="description" content="Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="abstract" content="Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="keywords" content="Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:site_name" content="Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta itemprop="name" content="Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<title>Contacto | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 
<?php include '../templates/favicon.php'; ?>
<?php include '../templates/fontsBlog.php'; ?>
<?php include '../templates/cssjsDetalleColeccion.php'; ?>
</head>

<body class="html not-front contacto menuar">

	<?php include '../templates/header.php';?>
<div class="page clearfix" id="page">


<section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix ">    
        
        <div class=" region region-content" id="region-content">
			  <div class="region-inner region-content-inner">

			    <h1 class="title" id="page-title">Contacto</h1>
				
				<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
					<div class="block-inner clearfix">
						<div class="content clearfix">
						
						<!--contenido-->
						
								<article class="node node-webform odd clearfix" id="node-webform-118">
									<div class="content clearfix">
									
									<form class="webform-client-form" enctype="multipart/form-data" action="enviar.php" method="post" id="webform-client-form-118" accept-charset="UTF-8">
									<div>
									
									<div class="form-item webform-component webform-component-textfield" id="webform-component-nombres">
									<label for="edit-submitted-nombres">Nombres <span class="form-required" title="Este campo es obligatorio.">*</span></label>
									<input type="text" id="edit-submitted-nombres" name="nombres" value="" size="60" maxlength="128" class="form-text" required>
									</div>
									
									<div class="form-item webform-component webform-component-textfield" id="webform-component-telefono">
									<label for="edit-submitted-telefono">Teléfono <span class="form-required" title="Este campo es obligatorio.">*</span></label>
									<input type="text" id="edit-submitted-telefono" name="telefono" value="" size="60" maxlength="128" class="form-text" required>
									</div>
									
									<div class="form-item webform-component webform-component-email" id="webform-component-direccion-de-correo-electronico">
									<label for="edit-submitted-direccion-de-correo-electronico">Dirección de correo electrónico <span class="form-required" title="Este campo es obligatorio.">*</span></label>
									<input class="email form-text form-email required" type="email" id="edit-submitted-direccion-de-correo-electronico" name="email" size="60" required>
									</div>
									
									<div class="form-item webform-component webform-component-textfield" id="webform-component-asunto">
									<label for="edit-submitted-asunto">Asunto <span class="form-required" title="Este campo es obligatorio.">*</span></label>
									<input type="text" id="edit-submitted-asunto" name="asunto" value="" size="60" maxlength="128" class="form-text" required>
									</div>
									
									<div class="form-item webform-component webform-component-textarea" id="webform-component-mensaje">
									<label for="edit-submitted-mensaje">Mensaje <span class="form-required" title="Este campo es obligatorio.">*</span></label>
									<div class="form-textarea-wrapper resizable textarea-processed resizable-textarea"><textarea id="edit-submitted-mensaje" name="mensaje" cols="60" rows="5" class="form-textarea" required></textarea><div class="grippie"></div></div>
									</div>
									
									<div class="form-actions form-wrapper" id="edit-actions">
									<input type="submit" id="edit-submit" name="op" value="Enviar" class="form-submit">
									</div>
									
									</div>
									</form>
									</div>
								</article>						
						
						<!--fin contenido-->
						</div>
						</div>
				</div>
				
				<div class="block block-block block-4 block-block-4" id="block-block-4">
					<div class="block-inner clearfix">
						<div class="content clearfix">
						<p><iframe height="465" src="https://mapsengine.google.com/map/embed?mid=z0TlfETc3xEM.kkS02yK7Ldl4&amp;z=17" width="640"></iframe></p>
						</div>
					</div>
				</div>
				
				
				<section class="block-view-block-puntos-venta-block block-views-view-block-puntos-venta-block odd" id="block-views-view-block-puntos-venta-block">
					<div class="block-inner clearfix">
					
					<h2 class="block-title">Puntos de Venta</h2>
					
					<div class="content clearfix">
						<div class="view view-view-block-puntos-venta view-id-view_block_puntos_venta view-display-id-block">
							<div class="view-content">
								
								<div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
								<div class="views-field views-field-title">
									<span class="field-content">Atelier- Showroom</span>
								</div>
								<div class="views-field views-field-body">
									<div class="field-content">
										<div style="color: rgb(0, 0, 0); font-family: HelveticaNeue, 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 16px;">Dirección: Cuesta del ángel 305 Yanahuara &nbsp;Arequipa, Peru.</div>
										<div style="color: rgb(0, 0, 0); font-family: HelveticaNeue, 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 16px;">Teléfonos: 0051- 54- 275444 cel. 0051-959111316.</div>
									</div>
								</div>
								</div>
							
							</div>
						</div>
					</div>
					
					</div>
				</section>

				
				</div>
				
				</div>
	
		</div>
	</div>
</section>    
  
</div>
<?php include '../templates/footer.php'; ?>
<?php include '../templates/scriptBeforeBody.php'; ?>
</body>
</html>