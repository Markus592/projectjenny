<?php $page = "index"; ?>
<?php include '../templates/config.php';
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../templates/meta.php'; ?>
	<?php include '../templates/favicon.php'; ?>

	<title>Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title>
	<?php include '../templates/fontsBlog.php'; ?>
	<?php include '../templates/cssjsDetalleColeccion.php'; ?>
</head>
<body class="html index page-principal">
	<div class="page clearfix" id="">
		<header id="section-header" class="section section-header">
			<div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">
				<div id="zone-branding" class="zone zone-branding clearfix ">
					<div class=" region region-branding" id="region-branding">
						<div class="region-inner region-branding-inner">

							<div class="block block-block block-1 block-block-1 odd block-without-title" id="block-block-1">
								<div class="block-inner clearfix">
									<div class="content clearfix">
										<p><a href="index"><img alt="" src="../images/logo_3.png" style="width: 170px; height: auto; margin:1rem 0" /></a></p>
									</div>
								</div>
							</div>

							<!-- slider -->
							<div class="divisorbanner">
								<ul class="bxslider">
									<!-- aqui se quito los titles -->
									<li class="port1"><a href="novia"><img src="<?= ROOT_PATH ?>banner/portada1.jpg" alt="Jenny Duarte NOVIAS Sesion de Fotos Aquarelle Modelo Daniela F1"  /></a></li>
									<li class="port2"><a href="joyeria"><img src="<?= ROOT_PATH ?>banner/portada2.jpg" alt="Jenny Duarte JOYAS Desfile Primavera verano 2014 Modelo 9"  /></a></li>
									<li class="port3"><a href="moda"><img src="<?= ROOT_PATH ?>banner/portada3.jpg" alt="Jenny Duarte COLECCI&Oacute;N ALPACA  Sesi	&oacute;n de fotos Oto&ntilde;o Invierno 2012 Modelo Soraya F3"  /></a></li>
									<li class="port4"><a href="altacostura"><img src="<?= ROOT_PATH ?>banner/portada4.jpg" alt="Jenny Duarte ALTA COSTURA Sesion de fotos Aquarelle Modelo Camila F2.jpg"  /></a></li>
								</ul>
							</div>
							<!-- End slider -->
							<!-- JSK: MENU PRINCIPAL-->
							<div class="block-menuprincipal" id="menuprincipal">
								<div class="block-inner clearfix">
									<ul class="slimmenu">
										<li><a class="limoda" href="moda">MODA</a></li>
										<li><a class="lialta" href="altacostura">ALTA COSTURA</a></li>
										<li><a class="linovia" href="novia">NOVIAS</a></li>
										<li><a class="lijoyeria" href="joyeria">JOYER&Iacute;A</a></li>
										<li><a class="" href="<?= ROOT_PATH ?>blog/es/blog">Blog</a></li>
										<li><a class="" href="contacto">CONTACTO</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<section id="section-content" class="section section-content">
			<div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">
				<div id="zone-content" class="zone zone-content clearfix ">
					<div class=" region region-content" id="region-content">
						<div class="region-inner region-content-inner">

							<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
								<div class="block-inner clearfix">
									<div class="content clearfix">

									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</section>

		<footer  class="section section-footer">
			<div class="region-inner region-footer-first-inner clearfix">

				<div class="block block-locale  block-language block-locale-language odd block-without-title" id="block-locale-language">
					<div class="block-inner clearfix">
						<div class="content clearfix">
							<!--<ul class="language-switcher-locale-url">
							<li class="en first"><a href="../en" class="language-link">English</a></li>
							<li class="es last active"><a href="../es" class="language-link active" xml:lang="es">Español</a></li>
						</ul> -->
						</div>
					</div>
				</div>

				<!-- JSK: MENU SECUNDARIO-->
				<div class="menu-secundario-footer-index">
					<div class="footer-section-idioma">
						<a href="/en">English</a>
						<a href="/es">Español</a>
					</div>
					<div class="footer-section-info">
						<a href="trayectoria-profesional">BIOGRAF&Iacute;A</a>
						<a href="contacto">CONTACTO</a>
					</div>
					<div class="footer-section-social-media">
						<a href="https://es-es.facebook.com/jennyduartemoda" target="_blank"><img src="../images/face.png" alt="facebook"></a>
						<a href="https://instagram.html" target="_blank"><img src="../images/instagram.png" alt="instagram"></a>
						<a href="https://www.twitter.com" target="_blank"><img src="../images/yticon.png" alt="youtube"></a>
						<a href="https://www.pinterest.com/jennyduarteperu/boards/" target="_blank">
						<img src="../images/icono_pinterest.png" alt="pinterest"></a>
						<!-- <a href="https://www.twitter.com" target="_blank"><img src="../images/twitter.png" alt=""></a> -->
					</div>
				</div>
				
			</div>
		</footer>

	</div>

	<script src="../js/jquery.slimmenu.js"></script>
	<script src="../js/jquery.easing.min.js"></script>

	<script>
		$(document).ready(function() {

			$('.bxslider').bxSlider({
				mode: 'fade',
				infiniteLoop: true,
				useCSS: true,
				touchEnabled: true,
				auto: true,
				speed: 700,
				adaptiveHeight: true,
				responsive: true,
				pager: true

			});
			$(document).ready(function() {
				function changecolorli() {

					if ($('.port1').css('display') != 'none') {
						// novia
						$('.linovia').css("color", "#d8b77c");

					} else {
						$('.linovia').css("color", "#767676");
					}
					if ($('.port2').css('display') != 'none') {
						// joyeria
						$('.lijoyeria').css("color", "#d8b77c");
					} else {
						$('.lijoyeria').css("color", "#767676");
					}
					if ($('.port3').css('display') != 'none') {
						// moda
						$('.limoda').css("color", "#d8b77c");

					} else {
						$('.limoda').css("color", "#767676");
					}
					if ($('.port4').css('display') != 'none') {
						// alta costura
						$('.lialta').css("color", "#d8b77c");
					} else {
						$('.lialta').css("color", "#767676");
					}
				}
				setInterval(changecolorli, 700);
			});
			jQuery(window).load(function() {

				$("li a.mostrar").click(function() {
					$(this).parent().find("ul").toggleClass("abrir")
				});
				$(".cerrar").click(function() {
					$("li ul.abrir").removeClass("abrir")
				});

			}); //fin load

			jQuery(window).resize(function() {

			}); //fin load

		}); //fin ready
	</script>
</body>

</html>