<?php $page = "joyeria"; ?>
<?php include '../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="description" content="Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="abstract" content="Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="keywords" content="Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:site_name" content="Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta itemprop="name" content="Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<link rel="shortcut icon" href="../images/favicon.png" type="image/png" />
  <title>Joyas | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 


<link type="text/css" rel="stylesheet" href="../css/global.css">

<link type="text/css" rel="stylesheet" href="../css/reset.css">
<link type="text/css" rel="stylesheet" href="../css/jsk.menu.css">
<link type="text/css" rel="stylesheet" href="../css/fonts.css">
<link type="text/css" rel="stylesheet" href="../css/jenny.css">
<link type="text/css" rel="stylesheet" href="../css/moda.css">
<link type="text/css" rel="stylesheet" href="../css/joyeria.css">
<link type="text/css" rel="stylesheet" href="../css/animate.min.css">
<script src="../js/jquery-1.11.3.min.js"></script>
<script src="../js/jquery.bxslider.js"></script>
<link href="../css/jquery.bxslider.css" rel="stylesheet" />
<script src="../js/jsk.menu.js"></script>	

<link rel="stylesheet" href="../css/jquery.slinky.css" />
<script src="../js/jquery.slinky.js"></script>
<!-- Facebook Pixel Code -->
<script>

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version="2.0";n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,"script","https://connect.facebook.net/en_US/fbevents.js");

fbq("init", "276600206390559", {

em: "insert_email_variable,"

});

fbq("track", "PageView");

</script>
<nonscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=276600206390559&ev=PageView&noscript=1"/>
</nonscript>
<!-- End Facebook Pixel Code -->
<script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
  <script src="../js/picturefill.min.js" async></script>
</head>

<body class="coleccion">
	<div id="mainWrapper">

		<?php include '../templates/header.php';?>

		<div class="hero wow fadeIn">
			<img data-src="imgjoyas/joya4.jpg">
		
			<h1>JOYAS </h1>
		</div>


		<main id="main">
		<div class="divisorespacios">&nbsp;</div>
				<div class="divisorbanner ">
	  			<ul class="bxslider">
				 		 <li><img src="imgjoyas/joya1.jpg" /></li>
        	  			  <li><img src="imgjoyas/joya2.jpg" /></li>
        	  			  <li><img src="imgjoyas/joya3.jpg" /></li>
        	  			  <li><img src="imgjoyas/joya5.jpg" /></li>
				  
	  			</ul>
	  			</div>
				  <div class="divisorespacios">&nbsp;</div>
				  <div class="divisorbanner ">
	  			<ul class="bxslider">
				  		  <li><img src="imgjoyas/soraya1.jpg" /></li>
        	  			  <li><img src="imgjoyas/soraya2.jpg" /></li>
        	  			  <li><img src="imgjoyas/soraya3.jpg" /></li>
				  
	  			</ul>
	  			</div>
				  
				 <div class="wow  fadeIn" id="grid1">
				

				<img class="wow bounceInLeft" data-wow-delay = "0.2s" id="img01" data-src="imgjoyas/aretes1.jpg">
				
				<img class="wow bounceInDown" data-wow-delay = "0.3s" id="img02" data-src="imgjoyas/aretes2.jpg">
				
				<img class="wow bounceInUp" data-wow-delay = "0.4s"   id="img03" data-src="imgjoyas/aretes3.jpg">
			</div>	
			<div class="divisorbanner ">
	  			<ul class="bxslider">
				  <li><img src="imgjoyas/joyas1.jpg"></li>
        	  			  <li><img src="imgjoyas/joyas2.jpg"></li>
        	  			  <li><img src="imgjoyas/joyas3.jpg"></li>
				  
	  			</ul>
	  			</div>

				



			

			
		</main>


		<?php include '../templates/footer.php';?>
	</div>
	<script src="../js/jquery.slimmenu.js"></script>
<script src="../js/jquery.easing.min.js"></script>

<script>
$(document).ready(function(){

$('.bxslider').bxSlider({
mode:'fade',
  infiniteLoop: true,
useCSS:true,
touchEnabled:true,
auto:true,
speed:700,
adaptiveHeight:true,
  responsive: true,
pager:true
});

jQuery(window).load(function() {
    
    $("li a.mostrar").click(function(){
        $(this).parent().find("ul").toggleClass("abrir")
    });
    $(".cerrar").click(function(){
        $("li ul.abrir").removeClass("abrir")
    });

}); //fin load

jQuery(window).resize(function() {

}); //fin load

});//fin ready

</script>

<script type="text/javascript" src = "../js/lazyload.min.js"> </script>
				<script type="text/javascript" src = "../js/index.js"> </script>
				<script type="text/javascript" src = "../js/wow.min.js"> </script> 
				<script>
				new WOW().init();
				</script>
</body>
</html>