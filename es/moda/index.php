<?php $page = "moda"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?> 
	<title>Sección Moda - Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../templates/cssjs.php';?> 
	<link type="text/css" rel="stylesheet" href="<?=ROOT_PATH?>css/portada.css">
	<script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );
  </script>
  <script src="<?=ROOT_PATH?>js/picturefill.min.js" async></script>
</head>

<body class="portadamoda">
	<div id="mainwrap">
		<?php include '../../templates/header.php';?>
		<aside id="aside">
			<h1>MODA</h1>
				<nav class="jd-vertical-menu">
					<!-- <button class="accordion ">Colecciones Actuales</button> -->
					<div class="actualacordeon">
						<a href="moda-otonoinvierno2020-21">OTOÑO INVIERNO 2020-21</a>
						<a href="moda-otonoinvierno2017-18">OTOÑO INVIERNO 2017-18</a>
						<a href="moda-otonoinvierno2015-16">OTOÑO INVIERNO 2015-16</a>
						<a href="moda-primaveraverano2014-15">PRIMAVERA VERANO 2014</a>
						<a href="moda-otonoinvierno2014-15">OTOÑO INVIERNO 2014-15</a>
						<a href="moda-otonoinvierno2013-14">OTOÑO INVIERNO 2013-14</a>
					</div>
					<button class="accordion">Archivo del 2008 al 2013</button>
					<div class="panel">
						<a href="moda-primaveraverano2012-13">PRIMAVERA VERANO 2012</a>
						<a href="moda-otonoinvierno2012-13">OTOÑO INVIERNO 2012-13 </a>
						<a href="moda-otonoinvierno2011-12">OTOÑO INVIERNO 2011-12</a>
						<a href="moda-otonoinvierno2010-11">OTOÑO INVIERNO 2010-11</a>
						<a href="moda-primaveraverano2010-11">PRIMAVERA VERANO 2010</a>
						<a href="moda-otonoinvierno2009-10">OTOÑO INVIERNO 2009-10</a>
						<a href="moda-otonoinvierno2008-09">OTOÑO INVIERNO 2008-09</a>
					</div>
				</nav>
		</aside>
		<main id="main">
			<img id="portadamodaimg" src="<?=ROOT_PATH?>images/moda/portada.jpg">
		</main>
		<?php include '../../templates/footer.php';?>
	</div>

	<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
	$("#mainwrap").css("grid-template-rows","10vh auto 10vh")
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
</body>
</html>