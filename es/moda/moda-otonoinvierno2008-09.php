<?php $page = "moda-coi2008"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php'; ?>
	<?php include '../../templates/favicon.php'; ?>
	<title>Aquarelle Coleccion otoño 2008 - Moda - Jenny Duarte Peru, Vestidos de novia,
		diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="../../css/moda-coi2008.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
	<link rel="stylesheet" href="../../css/video-js.css">
	<link rel="stylesheet" href="../../css/estilos.css">
	<script src="../../js/video.js"></script>
</head>

<body class="coleccion">
	<?php include '../../templates/header.php'; ?>
	<div id="page">
		<div class="hero  lazy">
			<h1 class="coi2008h1">COLECCIÓN OTOÑO INVIERNO 2008 </h1>
			<div id="hero_modacoi2008">
				<img class="wow fadeIn " data-wow-delay="0.5s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada1.jpg">
				<img class="wow fadeIn" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada2.jpg">
				<img class="wow fadeIn" data-wow-delay="0.3s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada3.jpg">
			</div>


		</div>
		<main id="main" class="container">
			<div class="medium wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg" />
			</div>
			<div class="threeimg">
				<img class="wow fadeIn " data-wow-delay="0.5s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/24.jpg" />
				<img class="wow fadeIn" data-wow-delay="0.75s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/25.jpg" />
				<img class="wow fadeIn" data-wow-delay="1s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/26.jpg" />
			</div>
			<div class="hero herethfassho2008 wow fadeIn" id="hero1">
				<!-- <img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg"> -->

				<h1>Desfile Paris, <br> Ethical Fashion Show 2008 </h1>

			</div>

			<div class="doble_medium doble lazy">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg" />
			</div>
			<div class="medium wow  rollIn lazy">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg" />
			</div>

			<div class="doble_medium_especial doble lazy">
				<img class=" wow  fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg" />
				<img class=" wow  fadeInRight" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg" />
			</div>
			<div class="doble_medium doble lazy">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/08.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg" />
			</div>
			<div class="doble_medium_especial doble lazy">
				<img class=" wow  fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg" />
				<img class=" wow  fadeInRight" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg" />
			</div>
			<div class="doble_medium doble lazy">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/13.jpg" />
			</div>
			<div class="doble_medium doble lazy">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg" />
			</div>
			<div class="doble_medium doble lazy">
				<!-- <img class=" wow  fadeInLeft" data-wow-delay="0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/16.jpg" /> -->
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg" />
			</div>
			<div class="doble_medium doble lazy">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg" />
			</div>
			<div class="doble_medium doble lazy">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/20.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/21.jpg" />
			</div>


			<div class="medium wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/22.jpg" />
			</div>
			<!-- <div class="medium_grand complet wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/23.jpg" />
			</div> -->


			<div class="contenedor video">
				<video class="fm-video video-js vjs-16-9 vjs-big-play-centered" poster="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/23.jpg" data-setup="{}" controls id="fm-video">
					<source src="../../video/media2008.mp4" type="video/mp4">
				</video>
				<p>Video del Desfile
					Ethical Fashion Show Paris 2008
				</p>

			</div>
			<?php include '../../templates/footer.php'; ?>




			<script type="text/javascript" src="../../js/lazyload.min.js"> </script>
			<script type="text/javascript" src="../../js/index.js"> </script>
			<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script>  -->
			<!-- <script>
				new WOW().init();
				</script> -->

</body>

</html>