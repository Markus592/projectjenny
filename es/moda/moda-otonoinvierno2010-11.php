<?php $page = "moda-coi2010"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php'; ?>
	<?php include '../../templates/favicon.php'; ?>
	<title>Moda Coleccion otono invierno 2010 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="../../css/moda-coi2010.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
	<link rel="stylesheet" href="../../css/video-js.css">
	<link rel="stylesheet" href="../../css/estilos.css">
	<script src="../../js/video.js"></script>
</head>

<body >
	<?php include '../../templates/header.php'; ?>
	<div id="page" class=<?=$page?>>


		<div class="hero wow fadeIn" id="hero_medium">
			<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada_22.jpg">

			<h1> COLECCION OTOÑO INVIERNO 2010 </h1>

		</div>


		<main id="main" class="container">

			<br>
			<div class="hero herodesfethfasshopar2010 wow fadeIn" id="">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01_05.jpg">

				<div class="info-desfile">
					<h1> DESFILE ETHICAL FASHION SHOW PARIS 2010</h1>
				</div>

			</div>

			<div class="doble_medium doble ">
				<img class=" wow position-top   fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg" />
				<img class=" wow position-top   fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg" />
			</div>


			<div class="doble_medium doble ">
				<img class=" wow position-top   fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg" />
				<img class=" wow  position-top fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01_05.jpg" />
			</div>
			<div class="doble_medium_especial doble ">
				<img class=" wow  position-top fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg" />
				<img class=" wow  position-top fadeInRight" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  position-top fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/08.jpg" />
				<img class=" wow  position-top fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  position-top fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg" />
				<img class=" wow  position-top fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg" />
			</div>
			<div class="doble_medium_especial doble ">
				<img class=" wow  position-top fadeInLeft" data-wow-delay="0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg" />
				<img class=" wow  position-top fadeInRight" data-wow-delay="0.4s" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/13.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  position-top fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14_23.jpg" />
				<img class=" wow  position-top fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft position-top" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg" />
				<!-- <img class=" wow  fadeInRight" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/20.jpg" /> -->
			</div>

			<div class="doble_medium doble">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/21.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada_22.jpg" />
			</div>

			<div class="contenedor">
				<video class="fm-video video-js vjs-16-9 vjs-big-play-centered" poster="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14_23.jpg" data-setup="{}" controls id="fm-video">
					<source src="../../video/media20101.mp4" type="video/mp4">
				</video>
				<div class="hero wow fadeIn" id="hero2">
					<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/24.jpg">

					<h1> DESFILE PERU MODA 2010 </h1>

				</div>
				<div class="doble_medium doble ">
					<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/25.jpg" />
					<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/26.jpg" />
				</div>

				<div class="doble_medium_especial doble">
					<img class=" wow  fadeInLeft" data-wow-delay="0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/27.jpg" />
					<img class=" wow  fadeInRight" data-wow-delay="0.4s" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/28.jpg" />
				</div>

				<div class="doble_medium doble">
					<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/29.jpg" />
					<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/30.jpg" />
				</div>

				<div class="doble_medium doble">
					<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/31.jpg" />
					<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/32.jpg" />
				</div>
				<div class="medium_grand wow  fadeIn">
					<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/33.jpg" />
				</div>
				<div class="medium_grand wow  fadeIn">
					<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/34.jpg" />
				</div>
				<div class="medium_grand wow  fadeIn">
					<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/35.jpg" />
				</div>


				<div class="contenedor">
					<video class="fm-video video-js vjs-16-9 vjs-big-play-centered" poster="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/36.jpg" data-setup="{}" controls id="fm-video">
						<source src="../../video/media2010.mp4" type="video/mp4">
					</video>


				</div>
				<?php include '../../templates/footer.php'; ?>
				<script type="text/javascript" src="../../js/lazyload.min.js"> </script>
				<script type="text/javascript" src="../../js/index.js"> </script>
				<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
				<script>
				new WOW().init();
				</script>
				 -->




				<!-- <script type="text/javascript" src = "../../js/video_1.js"> </script>				 -->
</body>

</html>