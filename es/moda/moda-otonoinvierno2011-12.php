<?php $page = "moda-coi2011"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?>
	<title>Moda Coleccion otono invierno 2011 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		 diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
		<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
	<link type="text/css" rel="stylesheet" href="../../css/video.css">
	<link rel="stylesheet" href="../../css/video-js.css">
	<link rel="stylesheet" href="../../css/estilos.css">
	<script src="../../js/video.js"></script>
</head>

<body class="">
	<?php include '../../templates/header.php';?>
	<div id="page">
		<div class="hero herootoinv2011">
			<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg">
			<h1>COLECCIÓN <br> OTOÑO INVIERNO <br> 2011 </h1>
		</div>
		<main id="main" class="container">
				
				
				<div class="doble_medium doble  ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg"/>
				</div>
				
				
				
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg"/>
				<img  class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg"/>
				</div>
				<div class="contenedor">
			<video class="fm-video video-js vjs-16-9 vjs-big-play-centered" poster="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg" data-setup="{}" controls id="fm-video">
				<source src="../../video/media2011.mp4" type="video/mp4">
			</video>
		<?php include '../../templates/footer.php';?>
				
			<script type="text/javascript" src = "../../js/lazyload.min.js"> </script>
			<script type="text/javascript" src = "../../js/index.js"> </script>
			<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
			<script>
			new WOW().init();
</script>	 -->
		
</body>
</html>