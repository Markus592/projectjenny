<?php $page = "moda-coi2013"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?>
	<title>Moda Coleccion otono invierno 2013 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		 diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
		<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="../../css/moda-coi2013.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css">
	 -->
</head>

<body class="">
	<?php include '../../templates/header.php';?>
	<div id="page">
		<div class="hero wow fadeIn herootoinv2013"  >
			<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg">
			<h1>COLECCIÓN OTOÑO INVIERNO 2013 </h1>
		</div>
		<main id="main" class="container">
		<div  class="medium wow  fadeIn ">
				   <img  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg"/>
				
				</div>
				<div  class="medium_grand wow fadeIn">
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg"/>
				
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg"/>
				</div>
				
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg"/>
				</div>
				
				<div class="medium_right wow  fadeIn"> 
				   <img  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg"/>
				</div>
				
				
				<div class="hero wow fadeIn">
					<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/08.jpg">
				
					<h1>DESFILE PERU MODA EN CHILE 2013  </h1>
					
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg"/>
				</div>
				<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg"/>
				
				</div>
				<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg"/>
				
				</div>
				<div  class="medium wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/13.jpg"/>
				
				</div>
				<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14.jpg"/>
				
				</div>
				<div  class="medium wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg"/>
				
				</div>
				<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/16.jpg"/>
				
				</div>
				<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg"/>
				
				</div>

				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg"/>
				</div>
				<div  class="medium wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/20.jpg"/>
				
				</div>

				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/21.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/22.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/23.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/24.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/25.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/26.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft"  data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/27.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/28.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft"  id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/28.jpg"/>
				<img class=" wow  fadeInRight" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/29.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/30.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/31.jpg"/>
				</div>
				
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/32.jpg"/>
				<img class=" wow  fadeInRight"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/33.jpg"/>
				</div>	
				<div  class="doble_medium_especial doble">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/34.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/35.jpg"/>
				</div>
				<div  class="medium wow  fadeIn">
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/36.jpg"/>
				</div>
				<?php include '../../templates/footer.php';?>
				<script type="text/javascript" src = "../../js/lazyload.min.js"> </script>
				<script type="text/javascript" src = "../../js/index.js"> </script>
				<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
				<script>
				new WOW().init();
				</script> -->
				
</body>
</html>