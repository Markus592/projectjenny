<?php $page = "moda-coi2014"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php'; ?>
	<?php include '../../templates/favicon.php'; ?>
	<title>Moda Coleccion otono invierno 2014 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="../../css/moda-coi2014.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
	<link rel="stylesheet" href="../../css/video-js.css">
	<link rel="stylesheet" href="../../css/estilos.css">
	<script src="../../js/video.js"></script>
</head>

<body class="">
	<?php include '../../templates/header.php'; ?>
	<div id="page">
		<div class="hero hero-coi2014 wow fadeIn">
			<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg">
			<h1>COLECCI&Oacute;N OTOÑO INVIERNO 2014 </h1>
		</div>
		<main id="main" class="container">
			<div class="wow fadeIn" id="grid1">
				<img id="img01" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg" />

				<img id="img02" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg" />


			</div>
			<div class="medium_grand wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg" />

			</div>

			<div class="medium wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg" />
			</div>

			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/08.jpg" />
			</div>
			<div class="doble_medium_especial doble">
				<img class=" wow  fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg" />
				<img class=" wow  fadeInRight" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/13.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14.jpg" />
			</div>
			<div class="doble_medium_especial doble  ">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" id="smallimage" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/16.jpg" />
			</div>
			<div class="medium wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg" />
			</div>
			<div class="medium_grand wow  fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg" />
			</div>


			<div class="contenedor">
				<video class="fm-video video-js vjs-16-9 vjs-big-play-centered" poster="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg" data-setup="{}" controls id="fm-video">
					<source src="../../video/media2014.mp4" type="video/mp4">
				</video>
				<?php include '../../templates/footer.php'; ?>
				<script type="text/javascript" src="../../js/lazyload.min.js"> </script>
				<script type="text/javascript" src="../../js/index.js"> </script>
				<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
				<script>
				new WOW().init();
				</script> -->
</body>
</html>