<?php $page = "moda-coi2015"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?>
	<title>Moda Coleccion otono invierno 2015 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		 diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
		<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="../../css/moda-coi2015.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
</head>

<body class="">
	<?php include '../../templates/header.php';?>
	<div id="page">
		<div class="hero wow fadeIn">
			<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg">
			<h1 class="titlemodacoi2015">COLECCIÓN <br> OTOÑO <br> INVIERNO 2015 </h1>
		</div>
		<main id="main" class="container">

		<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg"/>
				
		</div>
		<div  class="medium_grand wow  fadeIn herootoinv2015" >
				<!-- <img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg"/> -->
				<h2 class="otoinv2015">COLECCIÓN OTOÑO INVIERNO 2015</h2>
		</div>
		<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/2_1.jpg"/>
				
		</div>

		<div  id="grid1" >
				<img class="wow bounceInDown" id="img01" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg"/>
				
				<img class="wow bounceInUp" id="img02" data-wow-delay = "0.3s" data-src="<?= ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/04.jpg"/>
				
				<img class="wow bounceInRight" id="img03" data-wow-delay = "0.2s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg"/>
				</div>
		<div class="hero wow fadeIn">
			<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/06.jpg">
		
			<h1 class="permod2015">DESFILE PERU MODA 2015 </h1>
			
		</div>
				
				
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/08.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft"  data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/13.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg"/>
				<img class=" wow  fadeInRight" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/16.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg"/>
				</div>
		<div  class="medium img19 wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg"/>
				
		</div>		
		<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/20.jpg"/>
				
		</div>	
		<?php include '../../templates/footer.php';?>
		<script type="text/javascript" src = "../../js/lazyload.min.js"> </script>
		<script type="text/javascript" src = "../../js/index.js"> </script>
				<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
				<script>
				new WOW().init();
				</script> -->
</body>
</html>