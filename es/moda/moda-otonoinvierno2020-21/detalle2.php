<?php $page = "moda"; ?>
<?php include '../../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
	<?php include '../../../templates/meta.php'; ?>
	<?php include '../../../templates/favicon.php'; ?>
	<?php include '../../../templates/fontsBlog.php'; ?>
  	<title>2020-2021 Otoño - Invierno | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 
	<?php include '../../../templates/cssjsDetalleColeccion.php'; ?>
</head>
<body class="html not-front fiestadet coleccion">
<div class="page clearfix" id="page">
<!-- <header id="section-header" class="section section-header">
  <div id="zone-branding-wrapper" class="zone-wrapper zone-branding-wrapper clearfix">  
  <div id="zone-branding" class="zone zone-branding clearfix ">
    <div class=" region region-branding" id="region-branding">
		<div class="region-inner region-branding-inner">
		</div>
	</div>
  </div>
  </div>
</header> -->

<section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix ">    
        
        <div class=" region region-content" id="region-content">
			  <div class="region-inner region-content-inner clearfix">

				<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
					<div class="block-inner clearfix">
						<div class="content clearfix">
						
						<!--contenido-->
						
						<!-- <h1 class="page-title">2020-2021 Otoño - Invierno</h1> -->
						
						<!--menu movil-->
						<!-- <span class="mmovil"></span>
						<div class="block-menumovil" id="menumovil" style="display:none;">
        				  <ul class="menumovil">
        					    <li><a href="../index">Inicio</a></li>
        						<li><a href="../novias">NOVIAS</a></li>
        						<li><a href="../vestidos-de-fiesta-coctel">VESTIDOS DE FIESTA & COCTEL</a></li>
        					    <li><a href="../colecciones">COLECCIONES</a></li>
        					    <li><a href="../joyas">JOYAS</a></li>
        					    <li class="blog"><a href="../../blog/es">Blog</a></li>
        					</ul>
        				  
        				</div> -->
						<!--fin menu movil-->
						
						<div class="block-breadcrumb" id="breadcrumb">
				  <div class="block-inner clearfix">
					<ul class="slimmenu">
					    <li><a href="../index">Jenny Duarte</a></li>
						<li><a href="../colecciones">colecciones</a>
						</li>
						<li class="last">2020-2021 Otoño - Invierno</li>
					</ul>
				  </div>
				</div>
				
				<div class="pagdet">
					    <div class="burbuja">
					    <a href="detalle1" >1</a>
					    <a href="detalle2" class="active">2</a>
					    <a href="detalle3">3</a>
					    	</div>
					    	<div class="tit">2020-2021 Otoño - Invierno</div>
					    	</div>
						<!-- slider -->
        	  			<ul class="bxslider">
        	  			  <li><a class="lightb" href="images/col7.jpg"><img src="images/col7.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/col8.jpg"><img src="images/col8.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/col9.jpg"><img src="images/col9.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/col10.jpg"><img src="images/col10.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/col11.jpg"><img src="images/col11.jpg" /></a></li>
        	  			  <li><a class="lightb" href="images/col12.jpg"><img src="images/col12.jpg" /></a></li>
        	  			</ul>
        	  			
        	  			<div id="bx-pager">
                          <a data-slide-index="0" href=""><img src="images/col7.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="1" href=""><img src="images/col8.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="2" href=""><img src="images/col9.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="3" href=""><img src="images/col10.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="4" href=""><img src="images/col11.jpg" /><span class="hover"></span></a>
                          <a data-slide-index="5" href=""><img src="images/col12.jpg" /><span class="hover"></span></a>
                        </div>
        	  			<!-- End slider -->
						
						
						<!--fin contenido-->
						
						</div>
					</div>
				</div>
				
				</div>
	
		</div>
	</div>
	</div>
</section>    
</div>
<!-- Init Footer Template -->
<?php include '../../../templates/footer.php'; ?>
		<!-- End Footer Template -->
	<!-- Init script-->
	<?php include '../../../templates/scriptBeforeBodyDetalleColeccion.php'; ?>
	<!-- End script-->
</body>
</html>