<?php $page = "moda"; ?>
<?php include '../../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../../templates/meta.php'; ?>
	<?php include '../../../templates/favicon.php'; ?>
	<title>Coleccion Otño invierno 2020-2021 | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title>
	<?php include '../../../templates/fontsBlog.php'; ?>	
	<?php include '../../../templates/cssjsDetalleColeccion.php';?> 
	
</head>
<body  class="html not-front colecciones">
	<?php include '../../../templates/header.php'; ?>

	<div class="page clearfix" id="page">
		<section id="section-content" class="section section-content">
			<div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">
				<div id="zone-content" class="zone zone-content clearfix ">
					<div class=" region region-content" id="region-content">
						<div class="region-inner region-content-inner">
							<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
								<div class="block-inner clearfix">
									<div class="content clearfix">
										<!--contenido-->
										<div class="block-breadcrumb" id="breadcrumb">
											<div class="block-inner clearfix">
												<ul class="slimmenu">
													<li><a href="../index">Jenny Duarte</a></li>
													<li class="last">Otoño-Invierno 2020-2021</li>
												</ul>
											</div>
										</div>
										<div class="portada"><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/casona.jpg"><span class="bajar"></span>
											<div class="flechaselector"> &nbsp;</div>
										</div>
										<div class="divisorespacios">&nbsp;</div>
										<!-- slider -->
										<div class="slideralpaca">
											<ul class="bxslider">
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/imagen7ok.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/imagen14ok.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/imagen15.jpg" /></li>
											</ul>
										</div>
										<!-- End slider -->

										<div class="divisorespacios">&nbsp;</div>

										<div class="portada2">
											<h2>Coleccion Otono Invierno 2020-2021 <a href="detalle1">Fotos del Desfile</a></h2>
											<a href="detalle1"><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/col16.jpg" /></a>
										</div>

										<div class="divisorespacios">&nbsp;</div>

										<div class="slideralpacaparis">
											<h2>Invierno en Paris</h2>
											<ul class="bxslider">
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/paris3.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/paris2.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/paris1.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/paris4.jpg" /></li>
											</ul>
										</div>
										<div class="divisorespacios">&nbsp;</div>

										<div class="video">
											<iframe src="https://player.vimeo.com/video/219439667" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

										</div>

										<div class="divisorespacios">&nbsp;</div>

										<div class="portada"><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/jennyduartevestidos.jpg"><span class="bajar"></span></div>

										<div class="divisorespacios">&nbsp;</div>


										<div class="slideralpacagrupales">

											<ul class="bxslider">
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/grupal4ok.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/grupal8ok.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/grupal1ok.jpg" /></li>
												<li><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/grupal2ok.jpg" /></li>
											</ul>
										</div>

										<div class="divisorespacios">&nbsp;</div>

										<div class="bloquevideos2 bloque">
											<div class="inner">
												<div class="vid1 vid"><iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fjennyduartemoda%2Fvideos%2F1398113290247664%2F&show_text=0&width=266&height=266" width="266" height="266" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe></div>
												<div class="vid2 vid"><iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fjennyduartemoda%2Fvideos%2F1393998303992496%2F&show_text=0&width=266" width="266" height="266" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe></div>
												<div class="vid3 vid"><iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fjennyduartemoda%2Fvideos%2F1393990300659963%2F&show_text=0&width=266" width="266" height="266" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe></div>
												<div class="vid4 vid"><iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fjennyduartemoda%2Fvideos%2F1393971673995159%2F&show_text=0&width=266" width="266" height="266" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe></div>
											</div>
										</div>
										<div class="divisorespacios">&nbsp;</div>

										<div class="video">
											<iframe src="https://player.vimeo.com/video/221088331" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


										</div>

										<div class="divisorespacios">&nbsp;</div>

										<!--fin contenido-->

										<div class="portada4"><img src="<?= ROOT_PATH ?>colecciones/imgcoleccion/fotofinal.jpg"><span class="bajar"></span></div>


									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</section>



	</div>
	<?php include '../../../templates/footer.php'; ?>
	<?php include '../../../templates/scriptBeforeBody.php'; ?>

	
</body>

</html>