<?php $page = "moda-cpv2010"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?>
	<title>Moda Coleccion Primaver Verano 2010 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		 diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
		<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="../../css/moda-cpv2010.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
</head>

<body class="">
	<?php include '../../templates/header.php';?>
	<div id="page">
		<div class="hero herocpv2010">
			<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg">
			<h1>COLECCIÓN <br> PRIMERA <br>  VERANO 2010 </h1>	
		</div>
		<main id="main" class="container">
		<div class="wow  fadeIn"  id="grid1" >
				<img class="wow bounceInLeft" data-wow-delay = "0.2s" id="img01" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg"/>
				
				<img class="wow bounceInDown" data-wow-delay = "0.3s" id="img02" data-src="<?= ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/02.jpg"/>
				
				<img class="wow bounceInUp" data-wow-delay = "0.4s" id="img03" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/03.jpg"/>
				</div>
				<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg"/>
				
				</div>
				
				
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" id="img05" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/08.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg"/>
				<img class=" wow  fadeInRight" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/13.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/16.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/20.jpg"/>
				</div>
				
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/21.jpg"/>
				
				</div>	
				<div  class="doble_medium_especial doble">
				<img class=" wow  fadeInLeft"  id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/21.jpg"/>
				<img class=" wow  fadeInRight" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/22.jpg"/>
				</div>
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/23.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/24.jpg"/>
				</div>
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft" id="img25" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/25.jpg"/>
				<img class=" wow  fadeInRight" id="img26" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/26.jpg"/>
				</div>
				<div class="medium wow  fadeIn">
				<img id="img27"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/27.jpg"/>
				</div>	
				<?php include '../../templates/footer.php';?>
				<script type="text/javascript" src = "../../js/lazyload.min.js"> </script>
				<script type="text/javascript" src = "../../js/index.js"> </script>
				<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
				<script>
				new WOW().init();
				</script> -->
				
</body>
</html>