<?php $page = "moda-cpv2014"; ?>
<?php $padre = "moda"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?>
	<title>Moda Coleccion Primaver Verano 2014 - Alta Costura - Jenny Duarte Peru, Vestidos de novia,
		 diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
		<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
</head>

<body class="">
	<?php include '../../templates/header.php';?>
	<div id="page">
		<div class="hero hero-cpv2014 wow fadeIn">
			<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/portada.jpg">
			<h1>COLECCION PRIMAVERA VERANO 2014 </h1>
		</div>
		<main id="main" class="container">
				<div  class="medium_grand wow  fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/01.jpg"/>
				
				</div>
				<div  class="medium_grand wow fadeIn" >
				<img   data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/02.jpg"/>
				
				</div>
				<div class="hero heroprimversantfashwee2014 wow fadeIn" id="heromedium">
					<img data-src="<?=ROOT_PATH?>images2/<?= $padre ?>/<?= $page ?>/03.jpg">
				
					<h1>DESFILE PRIMAVERA VERANO SANTIAGO FASHION WEEK 2014 </h1>
			
				</div>

				
				
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/04.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/05.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/06.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/07.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/08.jpg"/>
				<img class=" wow  fadeInRight" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/09.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/10.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/11.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/12.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/13.jpg"/>
				</div>
				<div class="doble_medium_especial doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/14.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/15.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/16.jpg"/>
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17.jpg"/>
				</div>
				<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/17_1.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/18.jpg"/>
				</div>
				
				<div  class="doble_medium_especial doble">
				<img class=" wow  fadeInLeft"  id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/19.jpg"/>
				<img class=" wow  fadeInRight" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/20.jpg"/>
				</div>	
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/21.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/22.jpg"/>
				</div>	
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/23.jpg"/>
				<img class=" wow  fadeInRight"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/24.jpg"/>
				</div>	
				<div  class="doble_medium_especial doble">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/25.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/26.jpg"/>
				</div>	
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/27.jpg"/>
				<img class=" wow  fadeInRight"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/28.jpg"/>
				</div>	
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/29.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/30.jpg"/>
				</div>	
				<div  class="doble_medium_especial doble">
				<img class=" wow  fadeInLeft" id="imagenormal" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/31.jpg"/>
				<img class=" wow  fadeInRight" id="smallimage"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/32.jpg"/>
				</div>	
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/33.jpg"/>
				<img class=" wow  fadeInRight" data-wow-delay = "0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/34.jpg"/>
				</div>	
				<div  class="doble_medium doble">
				<img class=" wow  fadeInLeft"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/35.jpg"/>
				<img class=" wow  fadeInRight"  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/36.jpg"/>
				</div>	
				
				<div class="medium_grand wow  fadeIn">
				<img  data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/37.jpg"/>
				</div>	
				<?php include '../../templates/footer.php';?>
				<script type="text/javascript" src = "../../js/lazyload.min.js"> </script>
				<script type="text/javascript" src = "../../js/index.js"> </script>
				<!-- <script type="text/javascript" src = "../../js/wow.min.js"> </script> 
				<script>
				new WOW().init();
				</script> -->
				
</body>
</html>