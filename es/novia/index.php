<?php $page = "novia"; ?>
<?php $padre = "novia"; ?>
<?php include '../../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php';?>
	<?php include '../../templates/favicon.php';?>
	<title> Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 
	<?php include '../../templates/cssjs.php';?>
</head>

<body class="portada">
	<div id="mainWrapper">
		<?php include '../../templates/header.php';?>
		<aside id="aside">
			<h1 class="title-novia"> NOVIAS </h1>
			<nav class="jd-vertical-menu">
				<a href="novias-altacostura2019-20">NOVIAS ALTA COSTURA 2019 - 20</a>
				<a href="novias-archivo">NOVIAS ARCHIVO</a>
			</nav>
		</aside>
		<main id="main">
			<img id="jdPortadaImg" src="<?=ROOT_PATH?>images2/novia/novia-novia2019/image18.jpg">
		</main>
		<?php include '../../templates/footer.php';?>
	</div>

</body>
</html>