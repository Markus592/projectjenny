<?php $page = "novia-novia2019"; ?>
<?php $padre = "novia"; ?>
<?php include '../../templates/config.php'; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">

<head>
	<?php include '../../templates/meta.php'; ?>
	<?php include '../../templates/favicon.php'; ?>
	<title>NOVIAS - Jenny Duarte Peru, Vestidos de novia,
		diseñadora de modas, alta costura, tejidos alpaca, fashion
		designer, fashion designer</title>
	<?php include '../../templates/cssjsDetalleColeccion.php'; ?>
	<link type="text/css" rel="stylesheet" href="../../css/novia-cna2019.css">
	<link type="text/css" rel="stylesheet" href="../../css/novias.css">
	<!-- <link type="text/css" rel="stylesheet" href="../../css/animate.min.css"> -->
</head>

<body class=" archivo-novia">
	<?php include '../../templates/header.php'; ?>
	<div id="page">
		<div class="hero hero-height-auto wow fadeIn">
			<img class="heronovia" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/portadaarchivonovia.jpg">
			<h1 class="color-black">NOVIAS </h1>
		</div>
		<main id="main" class="container">
			<div class="medium wow fadeIn">
				<img id="image3" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image3.jpg" />

			</div>

			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image4.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image5.jpg" />
			</div>
			<div class="medium_grand complet wow fadeIn">
				<img id=image06 data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image6.jpg" />

			</div>
			<div class="medium_grand complet wow fadeIn">
				<img id=image07 data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image7.jpg" />

			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image8.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image9.jpg" />
			</div>
			<div class="medium_grand complet wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image10.jpg" />

			</div>

			<!-- slider -->
			<div class="nofloat">

				<ul class="bxslider">
					<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/bnovia1.jpg" /></li>
					<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/bnovia2.jpg" /></li>
					<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/bnovia3.jpg" /></li>
					<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/bnovia4.jpg" /></li>
				</ul>
			</div>

			<!-- End slider -->
			<!-- hay un bxsilder -->
			<div class="bloque2 grid1">
				<div class="imagen3 imagen"><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image17.jpg"></div>
				<div class="imagen4 imagen"><img class="imagenbloque1" src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image15.jpeg">
					<img class="imagenbloque2" src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image16.jpeg"></div>
			</div>

			<!-- <div id="grid1">
				<img class="wow bounceInLeft" data-wow-delay="0.2s" id="img01" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image17.jpg">

				<img class="wow bounceInDown" data-wow-delay="0.3s" id="img02" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image15.jpeg">

				<img class="wow bounceInUp" data-wow-delay="0.4s" id="img03" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image16.jpeg">
			</div> -->

			<div class="medium_grand image18 wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image18.jpg" />

			</div>
			<div class="medium_grand wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image19.jpg" />

			</div>
			<div class="medium_grand left small wow fadeIn image20">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image20.jpg" />

			</div>
			<div class="bloque2_3 bloque nofloat">
				<div class="inner2 clearfix">
					<!-- slider -->
					<ul class="bxslider">
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/novia2_b1.jpg"></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/novia2_b2.jpg"></li>
						<li><img src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/novia2_b3.jpg"></li>
					</ul>
					<!-- End slider -->

				</div>
			</div>
			<div class="medium_grand wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image21.jpg" />

			</div>

			<!-- bxslider -->
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image25.jpg" />
				<img class=" wow  fadeInRight" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image26.jpg" />
			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" id="image27" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image27.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" id="image28" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image28.jpeg" />
			</div>
			<div class="medium wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image29.jpg" />

			</div>
			<div class="doble_medium doble ">
				<img class=" wow  fadeInLeft" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image30.jpg" />
				<img class=" wow  fadeInRight" data-wow-delay="0.4s" data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image31.jpg" />
			</div>
			<div class="medium wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image32.jpg" />

			</div>
			<div class="medium_grand wow fadeIn">
				<img data-src="<?= ROOT_PATH ?>images2/<?= $padre ?>/<?= $page ?>/image33.jpg" />

			</div>
		</main>
		<?php include '../../templates/footer.php'; ?>
		<?php include '../../templates/scriptBeforeBody.php'; ?>
</body>
</html>