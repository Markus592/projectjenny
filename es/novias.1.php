<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="description" content="Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="abstract" content="Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="keywords" content="Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:site_name" content="Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta itemprop="name" content="Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<link rel="shortcut icon" href="../images/favicon.png" type="image/png" />
  <title>Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 

<link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/Aller_Bold-normal-bold/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/Aller_Light-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/Aller_Light_Italic-italic-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/Dotum-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/Halo_handletter-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/Neoretrofill-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/Notera-normal-normal/stylesheet.css">
 <link type="text/css" rel="stylesheet" href="../blog/sites/default/files/fontyourface/local_fonts/rage_italic-italic-normal/stylesheet.css">

<link type="text/css" rel="stylesheet" href="../css/global.css">

<link type="text/css" rel="stylesheet" href="../css/reset.css">
<link type="text/css" rel="stylesheet" href="../css/jsk.menu.css">
<link type="text/css" rel="stylesheet" href="../css/fonts.css">
<link type="text/css" rel="stylesheet" href="../css/jenny.css">
<link type="text/css" rel="stylesheet" href="../css/moda.css">



<script src="../js/jquery-1.11.3.min.js"></script>
<script src="../js/jquery.bxslider.js"></script>
<link href="../css/jquery.bxslider.css" rel="stylesheet" />
<script src="../js/jsk.menu.js"></script>	

<link rel="stylesheet" href="../css/jquery.slinky.css" />
<script src="../js/jquery.slinky.js"></script>
<!-- Facebook Pixel Code -->
<script>

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version="2.0′;n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,"script","https://connect.facebook.net/en_US/fbevents.js");
fbq("init", "276600206390559", {
em: "insert_email_variable,"
});
fbq("track", "PageView");

</script>
<nonscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=276600206390559&ev=PageView&noscript=1"/>
</nonscript>
<!-- End Facebook Pixel Code -->
</head>

<body class="html not-front novias">

<div class="page clearfix" id="page">

		<header id="jdHeader">
				<a href="index"><img alt="" src="../images/logo_3.png" style="width: 150px; height: auto;" /></a>
				
				<button class="hamburger hamburger--minus" type="button">
						<span class="hamburger-box">
						  <span class="hamburger-inner"></span>
						</span>
				</button>
				<nav role="navigation" class="jd-menu jsk-responsive-menu-wrapper">
					<a href="moda">MODA</a>
					<a href="altacostura">ALTA COSTURA</a>
					<a class="jd-active" href="novias">NOVIAS</a>
					<a href="joyeria">JOYER&Iacute;A</a>
					<a href="http://www.jennyduarteperu.com/blog/es/blog">BLOG</a>
				</nav>


	
		</header>

<section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix ">    
        
        <div class=" region region-content" id="region-content">
			  <div class="region-inner region-content-inner">

				<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
					<div class="block-inner clearfix">
						<div class="content clearfix">
						
						<!--contenido-->
						
						<h1 class="page-title">Novias</h1>
						
						<!--menu movil-->
						<span class="mmovil"></span>
						<div class="block-menumovil" id="menumovil" style="display:none;">
        				  <ul class="menumovil">
        					    <li><a href="index">Inicio</a></li>
        						<li><a href="novias">NOVIAS</a></li>
        						<li><a href="vestidos-de-fiesta-coctel">VESTIDOS DE FIESTA & COCTEL</a></li>
        					    <li><a href="colecciones">COLECCIONES</a></li>
        					    <li><a href="joyas">JOYAS</a></li>
        					    <li class="blog"><a href="../blog/es">Blog</a></li>
        					</ul>
        				  
        				</div>
						<!--fin menu movil-->
						
						<div class="block-breadcrumb" id="breadcrumb">
				  <div class="block-inner clearfix">
					<ul class="slimmenu">
					    <li><a href="index">Jenny Duarte</a></li>
						<li class="last">NOVIAS</li>
					</ul>
				  </div>
				</div>
				
						<div class="portada"><img src="imagesnovias/portadanovia1.jpg"><span class="bajar"></span><div class="flechaselector"> &nbsp;</div></div>
						
						<div class="divisorespacios">&nbsp;</div>
						<div class="bloque1 bloque">
						    <div class="inner">
						    <div class="imagen1 imagen"><img src="imagesnovias/lado1.jpg" ></div>
						    <div class="imagen2 imagen"><img src="imagesnovias/lado2.jpg"></div>
						    </div>
						</div>
						<div class="divisorespacios">&nbsp;</div>
						<div class="portada2"><img src="imagesnovias/portadanovia2ok.jpg"><span class="bajar"></span></div>
						<div class="divisorespacios">&nbsp;</div>
						<!-- slider -->
						<div id="slide1">
        	  			<ul class="bxslider ">
        	  			  <li><img src="imagesnovias/bnovia1.jpg" /></li>
        	  			  <li><img src="imagesnovias/bnovia2.jpg" /></li>
        	  			  <li><img src="imagesnovias/bnovia3.jpg" /></li>
        	  			  <li><img src="imagesnovias/bnovia4.jpg" /></li>
        	  			</ul>
        	  			</div>
        	  			<div class="divisorespacios">&nbsp;</div>
        	  			<!-- End slider -->
        	  			
        	  			<div class="bloque2 bloque">
						    <div class="imagen3 imagen"><img src="imagesnovias/novias03.jpg"></div>
						    <div class="imagen4 imagen"><img class="imagenbloque1" src="imagesnovias/novias02.jpg">
						    <img class="imagenbloque2" src="imagesnovias/novias01.jpg"></div>
						    
						</div>
							<div class="divisorespacios">&nbsp;</div>
						
						<div class="bloque3 bloque">
						    <div class="imagen4 imagen"><img src="imagesnovias/0024.jpg"></div>
						</div>
						
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque2_1 bloque">
						    <div class="inner2">
						    <img src="imagesnovias/noviaacercacimento.jpg">
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
        	  			<div class="bloque2_2 bloque">
						    <div class="inner">
						    <div class="imagen2 imagen"><img src="imagesnovias/noviakristel.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque2_3 bloque">
						    <div class="inner2 clearfix">
						        	<!-- slider -->
        	  			<ul class="bxslider">
        	  			  <li><img src="imagesnovias/novia2_b1.jpg"></li>
        	  			  <li><img src="imagesnovias/novia2_b2.jpg"></li>
        	  			  <li><img src="imagesnovias/novia2_b3.jpg"></li>
        	  			</ul>
        	  			<!-- End slider -->
						    
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque2_4 bloque">
						    <div class="inner">
						    <div class="imagen4 imagen"><img src="imagesnovias/novia2_4.jpg"></div>
						    <div class="imagen5 imagen"><img src="imagesnovias/novia2_5.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_2 bloque">
						    <div class="inner">
						    <div class="imagen3 imagen"><img src="imagesnovias/novia3_3.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_3 bloque">
						    <div class="inner clearfix">
						    <div class="imagen4 imagen"><img src="imagesnovias/novia3_4.jpg"></div>
						    <div class="imagen5 imagen"><img src="imagesnovias/novia3_5.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_4 bloque">
						    <div class="inner">
						    <div class="imagen6 imagen"><img src="imagesnovias/novia3_6.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_5 bloque">
						    <div class="inner">
						    <div class="imagen7 imagen"><img src="imagesnovias/novia3_7.jpg"></div>
						    </div>
						</div>
						
						<!--fin contenido-->
						
						</div>
					</div>
				</div>
				
				</div>
	
		</div>
	</div>
	</div>
</section>    
<!--
<footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix ">
    <div class=" region region-footer-first" id="region-footer-first">
		<div class="region-inner region-footer-first-inner clearfix">
			
			<div class="block block-locale  block-language block-locale-language odd block-without-title" id="block-locale-language">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<ul class="language-switcher-locale-url">
							<li class="en first"><a href="../en/novias" class="language-link">English</a></li>
							<li class="es last active"><a href="../novias" class="language-link active" xml:lang="es">Español</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="block block-nice-menus  block-2 block-nice-menus-2 even block-without-title" id="block-nice-menus-2">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<ul class="nice-menu nice-menu-down nice-menu-menu-menu-secundario" id="nice-menu-2">
							<li class="menu odd first"><a href="trayectoria-profesional" title="">Biografía</a></li>
							<li class="menu even "><a href="moda-sustentable">Moda Sustentable</a></li>
							<li class="menu odd last"><a href="contacto" title="Dejanos un mensaje.">Contacto</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="block block-block  block-2 block-block-2 odd block-without-title" id="block-block-2">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<p class="face im"><a href="https://www.pinterest.com" target="_blank">
							<img alt="" src="../images/icono_pinterest.png"  /></a>
						</p>
						
						<p class="face im"><a href="https://www.twitter.com" target="_blank">
							<img alt="" src="../images/twitter.png"  /></a>
						</p>
						
						<p class="face im"><a href="https://es-es.facebook.com/jennyduartemoda" target="_blank">
							<img alt="" src="../images/face.png" /></a>
						</p>
						
							<p class="face im"><a href="https://instagram.html" target="_blank">
							<img alt="" src="../images/instagram.png" /></a>
						</p>
						
						<p class="comprar im"><a href="http://web.iasoftgroup.com/jenny2017/es/cart">
							<img alt="" src="../images/tienda.png"  /></a>
						</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
  </div>
  </div>
</footer> -->

<?php include 'footer.php';?>

</div>

<script src="../js/jquery.slimmenu.js"></script>
<script src="../js/jquery.easing.min.js"></script>
<script>
$(document).ready(function(){

var slinky = $('#menumovil').slinky();


$(".mmovil").click(function(){
        $(this).toggleClass("open");
        $("#menumovil").toggleClass("abrir");
        if($("#menumovil").hasClass("abrir")){
            $("#menumovil").css("display","block");
        }
        else{
            $("#menumovil").css("display","none");
        }
    });

$('.bxslider').bxSlider({
mode:'fade',
  infiniteLoop: true,
useCSS:true,
touchEnabled:true,
auto:true,
speed:700,
adaptiveHeight:true,
  responsive: true,
pager:true
});

$(window).load(function() {

var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}

$("li a.mostrar").click(function(){
        $(this).parent().find("ul").toggleClass("abrir")
    });
    $(".cerrar").click(function(){
        $("li ul.abrir").removeClass("abrir")
    });
    
}); //fin load

$(window).resize(function() {

	var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
	if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}

}); //fin resize

});//fin ready

</script>
</body>
</html>