<?php $page = "novias"; ?>
<?php include '../templates/config.php';?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
	<?php include '../templates/meta.php';?>
	<?php include '../templates/favicon.php';?>
	<title>Novias | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 
	<link type="text/css" rel="stylesheet" href="../css/global.css">
	<?php include '../templates/cssjs.php';?>

	<link rel="stylesheet" href="../css/jquery.slinky.css" />
<script src="../js/jquery.slinky.js"></script>

</head>

<body class="html not-front novias">

<div class="page clearfix" id="page">

<?php include '../templates/header.php';?>

<section id="section-content" class="section section-content">
  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
  <div id="zone-content" class="zone zone-content clearfix ">    
        
        <div class=" region region-content" id="region-content">
			  <div class="region-inner region-content-inner">

				<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
					<div class="block-inner clearfix">
						<div class="content clearfix">
						
						<!--contenido-->
						
						<h1 class="page-title">Novias</h1>
						

						
						<div class="block-breadcrumb" id="breadcrumb">
				  <div class="block-inner clearfix">
					<ul class="slimmenu">
					    <li><a href="index">Jenny Duarte</a></li>
						<li class="last">NOVIAS</li>
					</ul>
				  </div>
				</div>
				
						<div class="portada"><img src="imagesnovias/portadanovia1.jpg"><span class="bajar"></span><div class="flechaselector"> &nbsp;</div></div>
						
						<div class="divisorespacios">&nbsp;</div>
						<div class="bloque1 bloque">
						    <div class="inner">
						    <div class="imagen1 imagen"><img src="imagesnovias/lado1.jpg" ></div>
						    <div class="imagen2 imagen"><img src="imagesnovias/lado2.jpg"></div>
						    </div>
						</div>
						<div class="divisorespacios">&nbsp;</div>
						<div class="portada2"><img src="imagesnovias/portadanovia2ok.jpg"><span class="bajar"></span></div>
						<div class="divisorespacios">&nbsp;</div>
						<!-- slider -->
						<div id="slide1">
        	  			<ul class="bxslider ">
        	  			  <li><img src="imagesnovias/bnovia1.jpg" /></li>
        	  			  <li><img src="imagesnovias/bnovia2.jpg" /></li>
        	  			  <li><img src="imagesnovias/bnovia3.jpg" /></li>
        	  			  <li><img src="imagesnovias/bnovia4.jpg" /></li>
        	  			</ul>
        	  			</div>
        	  			<div class="divisorespacios">&nbsp;</div>
        	  			<!-- End slider -->
        	  			
        	  			<div class="bloque2 bloque">
						    <div class="imagen3 imagen"><img src="imagesnovias/novias03.jpg"></div>
						    <div class="imagen4 imagen"><img class="imagenbloque1" src="imagesnovias/novias02.jpg">
						    <img class="imagenbloque2" src="imagesnovias/novias01.jpg"></div>
						    
						</div>
							<div class="divisorespacios">&nbsp;</div>
						
						<div class="bloque3 bloque">
						    <div class="imagen4 imagen"><img src="imagesnovias/0024.jpg"></div>
						</div>
						
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque2_1 bloque">
						    <div class="inner2">
						    <img src="imagesnovias/noviaacercacimento.jpg">
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
        	  			<div class="bloque2_2 bloque">
						    <div class="inner">
						    <div class="imagen2 imagen"><img src="imagesnovias/noviakristel.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque2_3 bloque">
						    <div class="inner2 clearfix">
						        	<!-- slider -->
        	  			<ul class="bxslider">
        	  			  <li><img src="imagesnovias/novia2_b1.jpg"></li>
        	  			  <li><img src="imagesnovias/novia2_b2.jpg"></li>
        	  			  <li><img src="imagesnovias/novia2_b3.jpg"></li>
        	  			</ul>
        	  			<!-- End slider -->
						    
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque2_4 bloque">
						    <div class="inner">
						    <div class="imagen4 imagen"><img src="imagesnovias/novia2_4.jpg"></div>
						    <div class="imagen5 imagen"><img src="imagesnovias/novia2_5.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_2 bloque">
						    <div class="inner">
						    <div class="imagen3 imagen"><img src="imagesnovias/novia3_3.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_3 bloque">
						    <div class="inner clearfix">
						    <div class="imagen4 imagen"><img src="imagesnovias/novia3_4.jpg"></div>
						    <div class="imagen5 imagen"><img src="imagesnovias/novia3_5.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_4 bloque">
						    <div class="inner">
						    <div class="imagen6 imagen"><img src="imagesnovias/novia3_6.jpg"></div>
						    </div>
						</div>
							<div class="divisorespacios">&nbsp;</div>
						<div class="bloque3_5 bloque">
						    <div class="inner">
						    <div class="imagen7 imagen"><img src="imagesnovias/novia3_7.jpg"></div>
						    </div>
						</div>
						
						<!--fin contenido-->
						
						</div>
					</div>
				</div>
				
				</div>
	
		</div>
	</div>
	</div>
</section>    
<!--
<footer id="section-footer" class="section section-footer">
  <div id="zone-footer-wrapper" class="zone-wrapper zone-footer-wrapper clearfix">  
  <div id="zone-footer" class="zone zone-footer clearfix ">
    <div class=" region region-footer-first" id="region-footer-first">
		<div class="region-inner region-footer-first-inner clearfix">
			
			<div class="block block-locale  block-language block-locale-language odd block-without-title" id="block-locale-language">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<ul class="language-switcher-locale-url">
							<li class="en first"><a href="../en/novias" class="language-link">English</a></li>
							<li class="es last active"><a href="../novias" class="language-link active" xml:lang="es">Español</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="block block-nice-menus  block-2 block-nice-menus-2 even block-without-title" id="block-nice-menus-2">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<ul class="nice-menu nice-menu-down nice-menu-menu-menu-secundario" id="nice-menu-2">
							<li class="menu odd first"><a href="trayectoria-profesional" title="">Biografía</a></li>
							<li class="menu even "><a href="moda-sustentable">Moda Sustentable</a></li>
							<li class="menu odd last"><a href="contacto" title="Dejanos un mensaje.">Contacto</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="block block-block  block-2 block-block-2 odd block-without-title" id="block-block-2">
				<div class="block-inner clearfix">
					<div class="content clearfix">
						<p class="face im"><a href="https://www.pinterest.com" target="_blank">
							<img alt="" src="../images/icono_pinterest.png"  /></a>
						</p>
						
						<p class="face im"><a href="https://www.twitter.com" target="_blank">
							<img alt="" src="../images/twitter.png"  /></a>
						</p>
						
						<p class="face im"><a href="https://es-es.facebook.com/jennyduartemoda" target="_blank">
							<img alt="" src="../images/face.png" /></a>
						</p>
						
							<p class="face im"><a href="https://instagram.html" target="_blank">
							<img alt="" src="../images/instagram.png" /></a>
						</p>
						
						<p class="comprar im"><a href="http://web.iasoftgroup.com/jenny2017/es/cart">
							<img alt="" src="../images/tienda.png"  /></a>
						</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
  </div>
  </div>
</footer> -->

<?php include '../templates/footer.php';?>

</div>

<script src="../js/jquery.slimmenu.js"></script>
<script src="../js/jquery.easing.min.js"></script>
<script>
$(document).ready(function(){
	
	var slinky = $('#menumovil').slinky();


$('.bxslider').bxSlider({
mode:'fade',
  infiniteLoop: true,
useCSS:true,
touchEnabled:true,
auto:true,
speed:700,
adaptiveHeight:true,
  responsive: true,
pager:true
});

$(window).load(function() {

var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}
    
}); //fin load

$(window).resize(function() {

	var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
	if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}

}); //fin resize

});//fin ready

</script>
</body>
</html>