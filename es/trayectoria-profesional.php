<?php include '../templates/config.php';?>
<?php $page = "trayectoria"; ?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="description" content="Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="abstract" content="Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="keywords" content="Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:site_name" content="Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta itemprop="name" content="Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<?php include '../templates/favicon.php'; ?>
<title>Trayectoria Profesional | Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer</title> 
<link type="text/css" href="../css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<?php include '../templates/cssjsDetalleColeccion.php'; ?>
<!-- the mousewheel plugin - optional to provide mousewheel support -->
<script type="text/javascript" src="../js/jquery.mousewheel.js"></script>
<!-- the jScrollPane script -->
<script type="text/javascript" src="../js/jquery.jscrollpane.min.js"></script>
<link rel="stylesheet" href="../css/jquery.slinky.css" />
<script src="../js/jquery.slinky.js"></script>


<?php include '../templates/fontsBlog.php'; ?>



</head>

<body class="html not-front node-type-biografia menuar">

	<?php include '../templates/header.php';?>
<div class="page clearfix" id="page">


<div id="">
	<section id="" class="section section-content">
	  <div id="zone-content-wrapper" class="zone-wrapper zone-content-wrapper clearfix">  
	  <div id="zone-content" class="zone zone-content clearfix ">    
	        
	        <div class=" region region-content" id="region-content">
				  <div class="region-inner region-content-inner">
					
					<div class="block block-system block-main block-system-main odd block-without-title" id="block-system-main">
						<div class="block-inner clearfix">
							<div class="content clearfix">
							
							<!--contenido-->
							
							<article class="node node-biografia odd clearfix" id="node-biografia-253">
								<div class="content clearfix">
								
									<div class="cont">
									<h1 class="title" id="page-title">Trayectoria Profesional</h1>
									
									<div class="field field-name-body field-type-text-with-summary field-label-hidden">
									<div class="field-items">
									<div class="field-item even">
									<p>Jenny Duarte,&nbsp;descubre su pasión&nbsp;y estudia&nbsp;diseño de modas&nbsp;en Paris en la escuela Fashion Forum.&nbsp;Hace sus practicas&nbsp;en&nbsp;la casa Torrente de&nbsp;esta misma ciudad.</p>
									<p>Luego regresa a Perú donde crea la marca Jenny Duarte con lineas de prêt-a-porter, alta costura y joyas.</p>
									<p><span style="display: none">&nbsp;</span></p>
									<ul>
										<li><em>SIMM -Salón Internacional de la moda en MADRID- 2006 y 2007, presentando colecciones elaboradas en alpaca y algodón peruano.</em><br>&nbsp;</li>
										<li><em>Museo de la Moda Galiera –PARIS en el 2007, colección en algodón pima, con bordados a mano del valle del colca y colección de joyas en plata y piedras naturales peruanas.</em><br>&nbsp;</li>
										<li><em>&nbsp;Ethical Fashion Show PARIS 2008 presentando su colección en alpaca, en un desfile de modas realizado en el carrusel del Louvre.</em><br>&nbsp;</li>
										<li><em>&nbsp;Pret a porter PARIS 2009, en el espacio Creative by donde presenta su colección otoño – invierno 2009-2010 de alpaca en un desfile.</em><br>&nbsp;</li>
										<li><em>&nbsp;Pret a porter PARIS 2010, presenta su colección otoño – invierno 2010 - 2011 alpaca en un desfile de modas.</em><br>&nbsp;</li>
										<li><em>&nbsp;Etical Fashion Show PARIS 2010, presentando la colección de primavera-verano 2011.</em><br>&nbsp;</li>
										<li><em>&nbsp;Etical Fashion Days SUIZA 2010, presentando la colección de primavera- verano 2011.</em><br>&nbsp;</li>
										<li><em>&nbsp;Mode las VEGAS 2011,presentando la colección de otoño-invierno 2011-2012.</em><br>&nbsp;</li>
										<li><em>&nbsp;Unicrea SUIZA 2011, presentando la colección de primavera – verano.</em><br>&nbsp;</li>
										<li><em>&nbsp;Unicrea SUIZA 2011, presentando la colección de otoño-invierno.</em><br>&nbsp;</li>
										<li><em>&nbsp;Premium BERLIN 2012, presentando la colección de otoño -invierno.</em><br>&nbsp;</li>
										<li><em>&nbsp;EXPO PERÚ 2013, en pasarela Perú Moda en Chile presentando la colección de otoño invierno.</em><br>&nbsp;</li>
										<li><em>&nbsp;SANTIAGO FASHION WEEK, 2013 presentando la colección de primavera verano en pasarela y stand.</em></li>
									</ul>
									<ul>
										<li>Peru Fashion Night NEW YORK 2014, presentando la colección otoño invierno 2013 -2014 en pasarela.</li>
									</ul>
									<h5><strong><em>Participación en salones de moda y desfiles nacionales de la línea prêt-a-porter&nbsp;:<span id="cke_bm_156E" style="display: none">&nbsp;</span></em></strong></h5>
									<ul>
										<li><em>PERÚ Moda </em><em>2009, 2010, 2011, 2012 en pasarela.</em></li>
										<li><em>Exhibe Perú, Lima </em><em>2010, presenta una colección elaborada por los artesanos de Puno Juliaca.</em></li>
										<li><em>Tejiendo la Pasarela, Lima </em><em>2012, presenta una colección elaborada por las artesanas del programa Las 10000 Mujeres.</em></li>
										<li><em>De nuestras manos</em><em>.</em><em> Lima </em><em>2012, presenta una colección elaborada por artesanas de la región Arequipa.</em></li>
									</ul>
									<h5><strong><em>Trabajos realizados de diseño y capacitaciones a grupos de artesanos:</em></strong></h5>
									<ul>
										<li>1997 y 1998 con la ONG&nbsp; El Taller, en diferentes talleres de Arequipa en el sector Textil y presentando las colecciones de alpaca en un desfile.</li>
										<li>1999 y 2000 con la ONG Hábitat, dando capacitaciones a grupos de artesanas y tejedoras.</li>
										<li>1999 desarrolló la colección primavera verano para Kantu del Grupo Inca.</li>
										<li>2010 diseño y consultoria para Cite Camélidos – Puno Juliaca, presentando la colección en un desfile en Exhibe&nbsp; Peru en Lima.</li>
										<li>2011 diseño y capacitacion a talleres de tejido de Arequipa, dentro del proyecto “10000 mujeres”, presentando la coleccion en Lima en el evento Tejiendo la Pasarela”.</li>
										<li>2012 diseño y desarrollo de colección con un grupo de mujeres artesanas de region Arequipa en la pasarela “ de Nuestras manos” organizado por Mincetur.</li>
										<li>2013 Diseño y desarrollo de colección para Orkocraft.</li>
									</ul>
									<h5><strong><em>Desfiles y eventos presentados en Arequipa de la línea de alta costura.</em></strong></h5>
									<ul>
										<li>Colección Transparences de vestidos de alta costura en el año 2000&nbsp; en&nbsp; la Alianza Francesa.</li>
										<li>Colección Turbulences de vestidos de alta costura&nbsp; en el Hotel Libertador en el año 2001.</li>
										<li>Colección Primeveres de vestidos de alta costura en&nbsp; el Hotel Libertador en el año 2002.</li>
										<li>Colección Rouge Baisser de vestidos de alta costura en el Hotel Libertador en el año 2003.</li>
										<li>Colección Belle Epoque de vestidos de alta costura en el Hotel Libertador&nbsp; en el año 2004.</li>
										<li>Colección Exotique - Alta costura y Exotique Alpaca en el Hotel Libertador en el año 2005.</li>
										<li>Colección Novias Jenny Duarte, en el Hotel Libertador en el año 2006.</li>
										<li>Colección Alta Costura y Alpaca Pret-a-Porter en el Monasterio de Santa Catalina en el año 2007.</li>
										<li>Colección de Alta Costura Claustros de la Compañía 2008.</li>
										<li>Colección de Alta Costura Fashion Night 2010– Club Internacional.</li>
									</ul>
									<h5><strong><em>Premiaciones:</em></strong></h5>
									<ul>
										<li>Premio Exporta fácil – Mayor número de exportaciones en la región Sur, Arequipa, Moquegua y Tacna 2010</li>
										<li>Premio Exporta fácil – Mayor número de exportaciones en la región Sur, Arequipa, Moquegua y Tacna 2011</li>
									</ul>
									</div>
									</div>
									</div>
									</div>
									
									<div class="field field-name-field-imagen field-type-image field-label-hidden">
										<!-- slider -->
										<ul class="bxslider">
										  <li><img src="<?=ROOT_PATH?>/images/trayectoria1.jpg" /></li>
										  <li><img src="<?=ROOT_PATH?>/images/trayectoria2.jpg" /></li>
										  <li><img src="<?=ROOT_PATH?>/images/trayectoria3.jpg" /></li>
										  <li><img src="<?=ROOT_PATH?>/images/trayectoria4.jpg" /></li>
										  <li><img src="<?=ROOT_PATH?>/images/trayectoria5.jpg" /></li>
										</ul>
										<!-- End slider -->
									</div>
									
								</div>
							</article>
	
							<!--fin contenido-->
							
							</div>
					</div>
					
									
					</div>
					
				  </div>
		
			</div>
		</div>
		</div>
	</section>   
</div> <!--aside-->
  
<footer  class="section section-footer">
			<div class="region-inner region-footer-first-inner clearfix">

				<div class="block block-locale  block-language block-locale-language odd block-without-title" id="block-locale-language">
					<div class="block-inner clearfix">
						<div class="content clearfix">
							<!--<ul class="language-switcher-locale-url">
							<li class="en first"><a href="../en" class="language-link">English</a></li>
							<li class="es last active"><a href="../es" class="language-link active" xml:lang="es">Español</a></li>
						</ul> -->
						</div>
					</div>
				</div>

				<!-- JSK: MENU SECUNDARIO-->
				<div class="menu-secundario-footer-index">
					<div class="footer-section-idioma">
						<a href="/en">English</a>
						<a href="/es">Español</a>
					</div>
					<div class="footer-section-info">
						<a href="trayectoria-profesional">BIOGRAF&Iacute;A</a>
						<a href="contacto">CONTACTO</a>
					</div>
					<div class="footer-section-social-media">
						<a href="https://es-es.facebook.com/jennyduartemoda" target="_blank"><img src="../images/face.png" alt="facebook"></a>
						<a href="https://instagram.html" target="_blank"><img src="../images/instagram.png" alt="instagram"></a>
						<a href="https://www.twitter.com" target="_blank"><img src="../images/yticon.png" alt="youtube"></a>
						<a href="https://www.pinterest.com/jennyduarteperu/boards/" target="_blank">
						<img src="../images/icono_pinterest.png" alt="pinterest"></a>
						<!-- <a href="https://www.twitter.com" target="_blank"><img src="../images/twitter.png" alt=""></a> -->
					</div>
				</div>
				
			</div>
		</footer> 

</div>

<script src="../js/jquery.slimmenu.js"></script>
<script src="../js/jquery.easing.min.js"></script>
<script>
$(document).ready(function(){

var slinky = $('#menumovil').slinky();
$(".mmovil").click(function(){
        $(this).toggleClass("open");
        $("#menumovil").toggleClass("abrir");
        if($("#menumovil").hasClass("abrir")){
            $("#menumovil").css("display","block");
        }
        else{
            $("#menumovil").css("display","none");
        }
    });

$('.bxslider').bxSlider({
mode:'fade',
  infiniteLoop: true,
useCSS:true,
touchEnabled:true,
auto:true,
speed:700,
adaptiveHeight:true,
  responsive: true,
pager:false
});

$(window).load(function() {

	$('.node-paginas .field-name-body, .node-biografia .field-name-body').jScrollPane();
	
	var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
	if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}
}); //fin load

$(window).resize(function() {

	var x=($(window).height()-$("#zone-content-wrapper").height())/2-61;
	if(x>0){
$("#zone-content-wrapper").css("padding-top",x).css("padding-bottom",x);
}

}); //fin load

});//fin ready

</script>
</body>
</html>