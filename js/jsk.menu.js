/* Toggle between adding and removing the "responsive" class to topnav when the user clicks on the icon */
jQuery(document).ready(function(){
  jQuery(".hamburger").click(function(){
    jQuery(".hamburger").toggleClass("is-active");
    jQuery(".jsk-responsive-menu-wrapper").toggleClass("responsive");
  });
  
});
