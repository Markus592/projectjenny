	<!-- aqui ponemos los estilos y js de detalle de algunas secciones de la pagina jenny duarte  -->
	<link rel="stylesheet" href="<?= ROOT_PATH ?>css/fonts.css" />
	<link rel="stylesheet" href="<?= ROOT_PATH ?>css/reset.css" />
	<link rel="stylesheet" href="<?= ROOT_PATH ?>css/jenny.css" />
	<link rel="stylesheet" href="<?= ROOT_PATH ?>css/portada.css" />
	<link rel="stylesheet" href="<?= ROOT_PATH ?>css/moda.css" />
	<link rel="stylesheet" href="<?= ROOT_PATH ?>css/menu.css" />
	<link type="text/css" rel="stylesheet" href="<?= ROOT_PATH?>css/jsk.menu.css">
	<link rel="stylesheet" href="<?= ROOT_PATH ?>colecciones/css/jquery.lightbox.css" />
	<link href="<?= ROOT_PATH ?>colecciones/css/jquery.bxslider.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?= ROOT_PATH ?>colecciones/css/jquery.slinky.css" />
	<link type="text/css" rel="stylesheet" href="<?= ROOT_PATH ?>colecciones/css/global.css">
	<script src="<?= ROOT_PATH ?>js/jquery-1.11.3.min.js"></script>
	<script src="<?= ROOT_PATH?>js/jsk.menu.js"></script>
	<script src="<?= ROOT_PATH?>js/menu-scroll.js"></script>		
	<script src="<?= ROOT_PATH ?>js/jquery.bxslider.js"></script>
	<script src="<?= ROOT_PATH ?>colecciones/js/jquery.slinky.js"></script>

	<!-- Facebook Pixel Code -->
	<script>
		!function (f, b, e, v, n, t, s) {
			if (f.fbq) return; n = f.fbq = function () {
				n.callMethod ?
				n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			}; if (!f._fbq) f._fbq = n;
			n.push = n; n.loaded = !0; n.version = "2.0"; n.queue = []; t = b.createElement(e); t.async = !0;
			t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
		}(window,
			document, "script", "https://connect.facebook.net/en_US/fbevents.js");
		fbq("init", "276600206390559", {
			em: "insert_email_variable,"
		});
		fbq("track", "PageView");
	</script>
	<nonscript>
		<img height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=276600206390559&ev=PageView&noscript=1" />
	</nonscript>
	<!-- End Facebook Pixel Code -->