<footer id="footer">
			<nav class="jd-menu-secundario" id="jsFooterIdioma">
				<a href="<?=ROOT_PATH?>en">English</a>
				<a href="<?=ROOT_PATH?>es">Español</a>

			</nav> 
			<nav class="jd-menu-secundario" id="jdFooterMenu">
				<a href="<?=ROOT_PATH?>/es/trayectoria-profesional" title="">BIOGRAF&Iacute;A</a>
				<a href="<?=ROOT_PATH?>/es/contacto" title="">CONTACTO</a>
				<!-- <a href="acercade">ACERCA DE</a> -->
				<!-- <a href="terminos">ENCU&Eacute;NTRANOS</a> -->
			</nav> 
			<nav class="jd-menu-secundario" id="jdDevMenu">
				<a  title="">DESARROLLADO POR: </a>
				<a href="http://www.almaquinta.com" target="_blank"><img src="<?= ROOT_PATH?>images/logoalma.png"></a>
				<!-- <a href="./"><img src="<?= ROOT_PATH?>images/jd.icon.png"></a> -->
				<a href="<?= ROOT_PATH?>"><img src="<?= ROOT_PATH?>images/favicon.png"></a>
			</nav>		
			<nav class="jd-menu-secundario" id="jdSocialMenu">
				<a href="https://www.facebook.com/jennyduartemoda/" target="_blank"><img class="xsmoll"  alt="" src="<?= ROOT_PATH?>images/face.png" /></a>
				<a href="https://www.instagram.com/jennyduarteperu/?hl=es-la" target="_blank"><img class="xsmoll"  alt="" src="<?= ROOT_PATH?>images/instagram.png" /></a>
				<a href="https://twitter.com/JennyDuartePeru" target="_blank"><img  class="xsmoll" alt="" src="<?= ROOT_PATH?>images/twitter.png" /></a>
				<a href="https://www.youtube.com/channel/UC89qKgHelXBwxD1LOtFeO4Q" target="_blank"><img  class="xsmoll" alt="" src="<?= ROOT_PATH?>images/yticon.png" /></a>
				<a href="https://www.pinterest.com/jennyduarteperu/boards/" target="_blank"><img  class="xsmoll" alt="" src="<?= ROOT_PATH?>images/icono_pinterest.png" /></a>
				
			</nav>
		</footer>