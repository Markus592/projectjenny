<footer id="footer">
			<nav class="jd-menu-secundario" id="jdFooterMenu">
				<a href="biografia" title="">BIOGRAPHY</a>
				<a href="acercade">ABOUT US </a>
				<a href="terminos">FIND US</a>
			</nav> 
			<nav class="jd-menu-secundario" id="jdDevMenu">
				<a href="biografia" title="">DEVELOPED BY </a>
				<a href="http://www.almaquinta.com" target="_blank"><img src="<?= ROOT_PATH?>images/logoalma.png"></a>
				<a href="./"><img src="<?= ROOT_PATH?>images/jd.icon.png"></a>
			</nav>		
			<nav class="jd-menu-secundario" id="jdSocialMenu">
				<a href="https://es-es.facebook.com/jennyduartemoda" target="_blank"><img alt="" src="<?= ROOT_PATH?>images/fb.icon.png" /></a>
				<a href="https://www.instagram.com/jennyduarteperu/" target="_blank"><img alt="" src="<?= ROOT_PATH?>images/ig.icon.png" /></a>
				<a href="https://twitter.com/JennyDuartePeru" target="_blank"><img alt="" src="<?= ROOT_PATH?>images/tt.icon.png" /></a>
			</nav>
		</footer>