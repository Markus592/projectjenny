<!-- google analitics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154880689-7"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-154880689-7');
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="description" content="Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="abstract" content="Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer, Pérou créateur de mode d&#039;alpaga" />
<meta name="keywords" content="Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:site_name" content="Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />
<meta itemprop="name" content="Jenny Duarte Peru, vestidos de novia, diseñadora de modas, alta costura, tejidos alpaca, fashion designer, fashion designer" />