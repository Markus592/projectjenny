<script src="<?= ROOT_PATH ?>colecciones/js/jquery.slimmenu.js"></script>
<script src="<?= ROOT_PATH ?>colecciones/js/jquery.easing.min.js"></script>
<script src="<?= ROOT_PATH ?>colecciones/js/jquery.lightbox.min.js"></script>
<script src="<?= ROOT_PATH ?>colecciones/js/jquery.slinky.js"></script>
<script>
	$(document).ready(function() {

		var slinky = $('#menumovil').slinky();


		$(".mmovil").click(function() {
			$(this).toggleClass("open");
			$("#menumovil").toggleClass("abrir");
			if ($("#menumovil").hasClass("abrir")) {
				$("#menumovil").css("display", "block");
			} else {
				$("#menumovil").css("display", "none");
			}
		});

		$('.bxslider').bxSlider({
			mode: 'fade',
			infiniteLoop: true,
			useCSS: true,
			touchEnabled: true,
			auto: true,
			speed: 700,
			adaptiveHeight: true,
			responsive: true,
			pager: true
		});

		$(window).load(function() {
			jQuery(".bloquevideos2.bloque iframe").height(jQuery(".bloquevideos2.bloque iframe").width());
			var x = ($(window).height() - $("#zone-content-wrapper").height()) / 2 - 61;
			if (x > 0) {
				$("#zone-content-wrapper").css("padding-top", x).css("padding-bottom", x);
			}

			$("li a.mostrar").click(function() {
				$(this).parent().find("ul").toggleClass("abrir")
			});
			$(".cerrar").click(function() {
				$("li ul.abrir").removeClass("abrir")
			});

		}); //fin load

		$(window).resize(function() {
			jQuery(".bloquevideos2.bloque iframe").height(jQuery(".bloquevideos2.bloque iframe").width());
			var x = ($(window).height() - $("#zone-content-wrapper").height()) / 2 - 61;
			if (x > 0) {
				$("#zone-content-wrapper").css("padding-top", x).css("padding-bottom", x);
			}

		}); //fin resize
		$('.lightb').lightbox();
	}); //fin ready
</script>
<script src="<?= ROOT_PATH ?>js/lazyload.min.js"></script>
<script src="<?= ROOT_PATH ?>js/index.js"></script>