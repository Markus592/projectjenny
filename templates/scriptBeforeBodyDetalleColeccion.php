<script src="<?= ROOT_PATH ?>colecciones/js/jquery.slimmenu.js"></script>
	<script src="<?= ROOT_PATH ?>colecciones/js/jquery.easing.min.js"></script>
	<script src="<?= ROOT_PATH ?>colecciones/js/jquery.lightbox.min.js"></script>
	<script>
		$(document).ready(function() {

			var slinky = $('#menumovil').slinky();


			$(".mmovil").click(function() {
				$(this).toggleClass("open");
				$("#menumovil").toggleClass("abrir");
				if ($("#menumovil").hasClass("abrir")) {
					$("#menumovil").css("display", "block");
				} else {
					$("#menumovil").css("display", "none");
				}
			});

			$('.bxslider').bxSlider({
				mode: 'fade',
				infiniteLoop: true,
				useCSS: true,
				touchEnabled: true,
				auto: true,
				speed: 700,
				adaptiveHeight: true,
				responsive: true,
				pagerCustom: '#bx-pager'
			});

			$(window).load(function() {

				var x = ($(window).height() - $("#zone-content-wrapper").height()) / 2 - 61;
				if (x > 0) {
					$("#zone-content-wrapper").css("padding-top", x).css("padding-bottom", x);
				} else {
					$("#zone-content-wrapper").css("padding-top", "").css("padding-bottom", "");
				}

				$("li a.mostrar").click(function() {
					$(this).parent().find("ul").toggleClass("abrir")
				});
				$(".cerrar").click(function() {
					$("li ul.abrir").removeClass("abrir")
				});

			}); //fin load

			$(window).resize(function() {

				var x = ($(window).height() - $("#zone-content-wrapper").height()) / 2 - 61;
				if (x > 0) {
					$("#zone-content-wrapper").css("padding-top", x).css("padding-bottom", x);
				} else {
					$("#zone-content-wrapper").css("padding-top", "").css("padding-bottom", "");
				}

			}); //fin resize

			$('.lightb').lightbox();

		}); //fin ready
	</script>